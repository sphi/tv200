{ root, pkgs, ... }:

rec {
  srcs = pkgs.linkFarm "webyeet" [
    { name = "wrangler.toml"; path = ./wrangler.toml; }
    { name = "webyeet.html"; path = ./webyeet.html; }
    { name = "worker.mjs"; path = ./worker.mjs; }
    { name = "public"; path = ./public; }
  ];

  # TODO: pin wrangler 3.16.0
  deploy = pkgs.writeShellScriptBin "deploy" ''
    set -e
    # initialise a dummy terraform dir
    SCRATCH="$(mktemp -d)"
    trap 'rm -rf -- "$SCRATCH"' EXIT

    cp -Lr ${srcs}/* $SCRATCH/

    cd $SCRATCH
    ${root.pkgsLatest.wrangler}/bin/wrangler deploy

    # set secrets
    # secret:bulk is broken with durable objects as of wrangler 3.22.1
    ${pkgs.sops}/bin/sops -d --extract '["SECRET_KEY"]' ${./secrets.yaml} \
      | ${root.pkgsLatest.wrangler}/bin/wrangler secret put SECRET_KEY
  '';

  tf = root.cloud.terraform.workspace "webyeet" {
    main = {
      resource.cloudflare_zone.webyeet = {
        provider = "cloudflare.global";
        paused = false;
        plan   = "free";
        type   = "full";
        zone   = "webyeet.com";
      };

      resource.cloudflare_zone_settings_override.settings = {
        zone_id = "\${cloudflare_zone.webyeet.id}";
        settings = [{
          always_use_https = "on";
        }];
      };

      resource.cloudflare_record.root = {
        provider = "cloudflare.global";
        name    = "webyeet.com";
        proxied = true;
        ttl     = 1;
        type    = "A";
        value   = "192.2.0.1"; # dummy
        zone_id = "\${cloudflare_zone.webyeet.id}";
      };

      resource.cloudflare_record.www = {
        name    = "www";
        proxied = true;
        ttl     = 1;
        type    = "A";
        value   = "192.2.0.1"; # dummy
        zone_id = "\${cloudflare_zone.webyeet.id}";
      };

      resource.cloudflare_page_rule.redirect_www = {
        priority = 1;
        status   = "active";
        target   = "www.webyeet.com/*";
        zone_id  = "\${cloudflare_zone.webyeet.id}";
        actions = [{
          forwarding_url = {
            status_code = 302;
            url         = "https://webyeet.com/$1";
          };
        }];
      };

      # add cors headers in a ruleset because the preview way we use static assets
      # doesnt support adding headers. also trying to proxy it in the worker
      # makes it hang...
      resource.cloudflare_ruleset.static_cors = {
        zone_id = "\${cloudflare_zone.webyeet.id}";
        name = "static_cors";
        description = "CORS for static assets";
        kind = "zone";
        phase = "http_response_headers_transform";

        rules = [{
          expression = "(http.request.uri.path eq \"/client.mjs\")";
          enabled = true;
          action = "rewrite";
          action_parameters = [{
            headers = [{
              operation = "set";
              name = "Access-Control-Allow-Origin";
              value = "*";
            }];
          }];
        }];
      };

      # worker proper managed by wrangler because cloudflare
      # terraform doesnt support durable objects
      resource.null_resource.deploy_worker = {
        triggers = {
          srcs = "${srcs}";
          secrets = ./secrets.yaml;
        };
        provisioner.local-exec.command = "exec ${deploy}/bin/deploy";
      };


      resource.cloudflare_worker_route.webyeet = {
        provider = "cloudflare.global";
        zone_id     = "\${cloudflare_zone.webyeet.id}";
        pattern     = "webyeet.com/*";
        script_name = "webyeet"; # managed by wrangler
      };
    };
  };
}