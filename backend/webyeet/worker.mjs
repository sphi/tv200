// Worker

import HTML from "./webyeet.html";

function dec2hex(dec) {
  return dec.toString(16).padStart(2, "0")
}

function generateToken(len) {
  var arr = new Uint8Array((len || 40) / 2)
  crypto.getRandomValues(arr)
  return Array.from(arr, dec2hex).join('')
}

function parseCookies(request) {
  let cookieString = request.headers.get('Cookie') || ''
  let cookies = {}
  cookieString.split(';').forEach(cookie => {
    let [name, value] = cookie.split('=').map(v => v.trim())
    cookies[name] = value
  })
  return cookies
}

async function sign(secret, data) {
  const encoder = new TextEncoder()
  const key = await crypto.subtle.importKey(
    'raw', 
    encoder.encode(secret),
    { name: 'HMAC', hash: 'SHA-256' },
    false,
    [ 'sign' ]
  )
  
  const signature = await crypto.subtle.sign('HMAC', key, encoder.encode(data))
  const base64signature = btoa(String.fromCharCode(...new Uint8Array(signature)))
  return base64signature;
}

async function verify(secret, data, signature) {
  const encoder = new TextEncoder()
  const key = await crypto.subtle.importKey(
    'raw', 
    encoder.encode(secret),
    { name: 'HMAC', hash: 'SHA-256' },
    false,
    [ 'verify' ]
  )
  
  const signatureBuffer = Uint8Array.from(atob(signature), c => c.charCodeAt(0))
  const verified = await crypto.subtle.verify('HMAC', key, signatureBuffer, encoder.encode(data))
  return verified;
}

async function getToken(secret, session) {
  if (!session) {
    return null;
  }
  // split the session in into token and signature
  let [ctoken, signature] = session.split("_");
  if (!await verify(secret, ctoken, signature)) {
    return null;
  }
  return ctoken;
}

export default {
  async fetch(request, env) {
    let url = new URL(request.url);
    let path = url.pathname.slice(1).split('/');

    if (path[0] == "new") {
      // API
      // new session
      let token = generateToken(8);
      let signature = await sign(env.SECRET_KEY, token);
      let session = `${token}_${signature}`;
      return new Response(JSON.stringify({
        token: token,
        session: session,
      }), {
        headers: {
          "Content-Type": "application/json;charset=UTF-8",
          "Access-Control-Allow-Origin": "*",
        }
      });
    }

    if (!path[0]) {
      // serve home page
      if (request.method != "GET") {
        return new Response("bad method", {status: 405});
      }

      // prevent leaking the session cookie from
      // same-origin requests to the home page
      let referer = request.headers.get("referer");
      if (referer) {
        let refererUrl = new URL(referer);
        if (refererUrl.hostname == url.hostname && refererUrl.pathname != "/") {
          return new Response("bad referer", {status: 403});
        }
      }
      if (request.headers.get("Sec-Fetch-Mode")
        && request.headers.get("Sec-Fetch-Mode") != "navigate") {
        return new Response("no fetch", {status: 403});
      }

      // home page
      let headers = {};
      let cookies = parseCookies(request);

      // try to re-use the session from the ookie
      let session = cookies["webpipe-session"];
      let token = await getToken(env.SECRET_KEY, session);
      if (!token) {
        // new session
        token = generateToken(8);
        let signature = await sign(env.SECRET_KEY, token);
        session = `${token}_${signature}`;
        if (url.protocol == "https:") {
          headers["Set-Cookie"] = `webpipe-session=${session}; Secure; HttpOnly; Max-Age=86400`;
        } else {
          headers["Set-Cookie"] = `webpipe-session=${session}; HttpOnly; Max-Age=86400`;
        }
      }

      headers["Content-Type"] = "text/html;charset=UTF-8";
      // prevent embedding and leaking our session
      headers["Content-Security-Policy"] = "frame-ancestors 'none';";
      
      let resp = HTML.replace("{{TOKEN}}", token);
      resp = resp.replace("{{SESSION}}", session);
      let response = new Response(resp, {headers: headers});
      
      return response;
    }

    let token = path[0];
    // 8 or 32 characters
    if (!token || !token.match(/^[0-9a-f]{8}$|[0-9a-f]{32}$/)) {
      return new Response("bad token", {status: 404});
    }
    let id = env.WEBPIPE.idFromName(token);
    let obj = env.WEBPIPE.get(id);

    return obj.fetch(request);
  }
}


// Durable Object
export class WebPipe {
  constructor(state, env) {
    this.state = state;
    this.env = env;

    let websockets = this.state.getWebSockets();
    this.ws = websockets.length > 0 ? websockets[0] : null;

    this.proxyHandlers = new Map();
  }

  async nextReqId() {
    let value = (await this.state.storage.get("requestNum")) || 0;
    await this.state.storage.put("requestNum", value+1);
    return value;
  }

  async fetch(request) {
    let url = new URL(request.url);

    // /token/blah
    let path = url.pathname.slice(1).split('/');

    if (path[1] == "websocket") {
      if (path.length != 2 || request.headers.get("Upgrade") != "websocket") {
        return new Response("expected websocket", {status: 400});
      }
      if (await getToken(this.env.SECRET_KEY, url.searchParams.get("session")) != path[0]) {
        return new Response("not authorized", {status: 401});
      }

      let pair = new WebSocketPair();

      // Accept our end of the WebSocket.
      this.ws = pair[1];
      this.state.acceptWebSocket(this.ws);

      return new Response(null, { status: 101, webSocket: pair[0] });      
    } else if (path[1] == "response") {
      // its a proxy response
      if (path.length != 3) {
        return new Response("expected response", {status: 400});
      }
      if (await getToken(this.env.SECRET_KEY, url.searchParams.get("session")) != path[0]) {
        return new Response("not authorized", {status: 401});
      }

      // cors
      let headers = {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "*",
        "Access-Control-Allow-Methods": "PUT, OPTIONS",
        "Access-Control-Max-Age": "86400",
      };
      if (request.method == "OPTIONS") {
        return new Response(null, {status: 200, headers: headers});
      }
      if (request.method != "PUT") {
        return new Response("bad method", {status: 405});
      }

      let reqId = parseInt(path[2]);

      let handler = this.proxyHandlers.get(reqId);
      if (!handler) {
        return new Response("no handler", {status: 400});
      }
      
      // handler(request.headers, await request.arrayBuffer());
      await handler(request);

      return new Response(null, {status: 200, headers: headers});
    }

    // proxy it
    if (!this.ws) {
      return new Response("no connection", {status: 404});
    }
    return this.proxyRequest(request)
  }

  proxyHeadersOutgoing(responseReq) {
    let headers = {};
    for (let [key, value] of responseReq.headers.entries()) {
      if (key.startsWith("x-proxy-header-")) {
        key = key.slice(15);
        // just spitballing
        if (key == "set-cookie"
          || key == "set-cookie2"
          || key == "cache-control"
          || key == "expires"
          || key == "strict-transport-security"
          || key == "content-security-policy"
          || key == "x-content-security-policy"
          || key == "x-webkit-csp"
          || key == "expect-ct"
          || key == "nel"
          || key == "report-to") {
          continue;
        }
        headers[key] = value;
      }
    }
    headers["X-Robots-Tag"] = "noindex, nofollow, noarchive, noimageindex";
    // prevent loading proxied scripts from adding service workers
    headers["Content-Security-Policy"] = "worker-src 'none'; manifest-src 'none';";
    return headers;
  }

  proxyHeadersIncoming(request) {
    const headers = {};
    for (const [key, value] of request.headers.entries()) {
      if (key == 'cookie') {
        continue;
      }
      headers[key] = value;
    }
    return headers;
  }

  async proxyRequest(request) {
    // Serialize the request body if it exists
    let body = null;
    if (request.method !== 'GET' && request.method !== 'HEAD') {
        body = await request.text();
    }

    let reqId = await this.nextReqId();

    // http://blah/token/path -> http://localhost
    let url = new URL(request.url);
    let path = url.pathname.slice(1).split("/");
    let newurl = "http://browser.invalid/" + path.slice(1).join("/");

    let reqSerialized = {
      id: reqId,
      method: request.method,
      url: newurl,
      headers: this.proxyHeadersIncoming(request),
      body: body,
    };

    this.ws.send(JSON.stringify(reqSerialized));

    let { readable, writable } = new TransformStream();

    return await (new Promise(resolve => {
      // give 10 seconds for the first chunk to arrive
      let chunkTimeout = setTimeout(() => {
        this.proxyHandlers.delete(reqId);
        resolve(new Response("timeout", {status: 504}));
      }, 10*1000);

      this.proxyHandlers.set(reqId, async (responseReq) => {

        clearTimeout(chunkTimeout);

        if (responseReq.headers.get("X-Proxy-Chunk-Number") == "0") {
          // first chunk, start the response
          resolve(new Response(readable, {
            status: responseReq.headers.get("X-Proxy-Status") || 400,
            headers: this.proxyHeadersOutgoing(responseReq),
          }));
        }

        let done = (responseReq.headers.get("X-Proxy-Chunk-Done") == "true")
        try {
          await responseReq.body.pipeTo(writable, {preventClose: !done});
        } catch (e) {
          console.log(e);
          this.proxyHandlers.delete(reqId);
        }

        if (done) {
          this.proxyHandlers.delete(reqId);
        } else {
          // 30 seconds timeout for the next chunk
          chunkTimeout = setTimeout(() => {
            this.proxyHandlers.delete(reqId);
            writable.abort("timeout");
          }, 30*1000);
        }

      })
    }));
  }

}
