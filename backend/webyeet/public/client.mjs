function delay(time) {
  return new Promise(resolve => setTimeout(resolve, time));
}

function makeChunkedReader(reader) {
  // https://stackoverflow.com/questions/70270997

  // only do 1mb chunks because cloudflare buffers all request data ><
  const chunkSize = 1 * 1024 * 1024 // 1MB

  let buffer = null;
  return new ReadableStream({
    async pull(controller) {
      let fulfilledChunkQuota = false;

      while (!fulfilledChunkQuota) {
        const {done, value} = await reader.read();

        if (!done) {
          buffer = new Uint8Array([...(buffer || []), ...value]);

          while (buffer.byteLength >= chunkSize) {
            const chunkToSend = buffer.slice(0, chunkSize);
            controller.enqueue(chunkToSend);
            buffer = new Uint8Array([...buffer.slice(chunkSize)]);
            fulfilledChunkQuota = true;
          }
        }
        if (done) {
          fulfilledChunkQuota = true;
          if (buffer && buffer.byteLength > 0) {
            controller.enqueue(buffer);
          }
          controller.close();
        }
      }
    }
  }).getReader();
}

export default class WebYeetFileServer {
  constructor(yeeturl, token, session, files) {
    this.yeeturl = new URL(yeeturl);

    this.token = token;
    this.session = session;
    this.files = files;

    this.ws = null;

    this.onAccess = null;
  }

  static async fromNewSession(yeeturl, files) {
    yeeturl = new URL(yeeturl)
    let resp = await (await fetch(`${yeeturl.origin}/new`)).json();
    return new WebYeetFileServer(yeeturl, resp.token, resp.session, files);
  }

  async connect() {
    const wss = (this.yeeturl.protocol == "http:") ? "ws://" : "wss://";
    this.ws = new WebSocket(
      `${wss}${this.yeeturl.host}/${this.token}/websocket?`+
      new URLSearchParams({session: this.session})
    );
  
    this.ws.addEventListener("message", async event => {
      let data = JSON.parse(event.data);
      console.log(data);
      await this.proxyHandler(data);
    });
  
    this.ws.addEventListener("close", async event => {
      console.log("reconnecting");
      await delay(1000);
      await this.connect();
    });

    // wait for connect
    await new Promise((resolve, reject) => {
      this.ws.addEventListener("open", resolve);
      this.ws.addEventListener("error", reject);
    });
  }

  async proxyHandler(requestData) {
    let request = new Request(requestData.url, requestData);
    let response = await this.handleRequest(request);
  
    if (this.onAccess) {
      this.onAccess(request, response);
    }
  
    let reader = makeChunkedReader(response.body.getReader());
    let chunkNum = 0;
  
    while (true) {
      const {done, value} = await reader.read();
  
      let chunkHeaders = {};
      chunkHeaders["X-Proxy-Chunk-Number"] = chunkNum;
      chunkHeaders["X-Proxy-Chunk-Done"] = done;
  
      if (chunkNum == 0) {
        // proxy the other headers in the first chunk only
        chunkHeaders["X-Proxy-Status"] = response.status;
        for (const [key, value] of response.headers.entries()) {
          chunkHeaders["X-Proxy-Header-"+key] = value;
        }
      }
  
      let chunkResponse = await fetch(
        `${this.yeeturl.origin}/${this.token}/response/${requestData.id}?`+(
          new URLSearchParams({session: this.session})
        ), {
          method: 'PUT',
          headers: chunkHeaders,
          body: value
        }
      );
  
      chunkNum += 1;
  
      if (chunkResponse.status != 200) {
        break;
      }
      if (done) break;
    }
  }

  async handleRequest(request) {
    let url = new URL(request.url);
  
    if (request.method != "GET") {
      return new Response("only get", {status: 400});
    }
  
    if (url.pathname == "/") {
      return new Response("hi");
    }
  
    for (let file of this.files) {
      if (decodeURIComponent(url.pathname) == "/" + file.name) {
        let status = 200;
        let headers = {
          "Accept-Ranges": "bytes",
        }
        if (request.headers.get("Range")) {
          let range = request.headers.get("Range");
          let match = range.match(/^bytes=(\d+)-(\d+)?$/);
          if (match) {
            let start = parseInt(match[1]);
            let end = parseInt(match[2] || file.size - 1);
            headers["Content-Range"] = "bytes " + start + "-" + end + "/" + file.size;
            headers["Content-Length"] = end - start + 1;
            status = 206;
            file = file.slice(start, end + 1);
          } else {
            return new Response("bad range", {status: 400});
          }
        } else {
          headers["Content-Length"] = file.size;
        }
        return new Response(file, {headers: headers, status: status});
      }
    }
  
    return new Response("not found", {status: 404});
  }
}