# webyeet

webyeet is a 3rd party service used for the file upload functionality. It should be deployed to [webyeet.com](https://webyeet.com/) using Cloudflare workers, in which case the code here isn't needed. Most of the code needed to deploy it is here, but actually getting it running would be a bit of a project. A newer version of the code may appear in the [TVL monorepo](https://code.tvl.fyi/tree/) at some point.
