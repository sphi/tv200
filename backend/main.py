#!/usr/bin/env python3

import server
import sys

if __name__ == '__main__':
    # Support both to match the warning sanic gives automatically when run in production mode
    debug = '--debug' in sys.argv or '--dev' in sys.argv
    server.run(debug)
