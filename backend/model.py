from typing import Optional
from abc import abstractmethod
import random
import time
import asyncio
import logging
import json
import repo

# Time in seconds before forgetting about device
device_expire_time = 60 * 60 * 24 * 7

class ConnectionErr(RuntimeError):
    def status(self) -> int:
        return 503

class RateLimitErr(ConnectionErr):
    def status(self) -> int:
        return 429

class MessageErr(RuntimeError):
    pass

class Pipe:
    def __init__(self) -> None:
        self._closed = False;

    async def send(self, data: dict) -> None:
        if not self._closed:
            await self.received(data)

    async def close(self, reason: str):
        if not self._closed:
            self._closed = True
            await self.closed(reason)

    @abstractmethod
    async def received(self, data) -> None:
        pass

    @abstractmethod
    async def closed(self, reason: str):
        pass

def generate_random_string(num: int, chars: list[str]) -> str:
    return ''.join(random.choice(chars) for i in range(num))

token_chars = (
    [chr(65 + i) for i in range(26)] +
    [chr(97 + i) for i in range(26)]
)
def generate_token() -> str:
    return generate_random_string(16, token_chars)

# omit O and 0 because they could be confused
pairing_code_chars = (
    [chr(65 + i) for i in range(26) if chr(65 + i) != 'O'] +
    [chr(48 + i) for i in range(10) if i != 0]
)
pairing_code_len = 4
stable_code_len = 8

def generate_pairing_code() -> str:
    return generate_random_string(pairing_code_len, pairing_code_chars)

def generate_stable_code() -> str:
    return generate_random_string(stable_code_len, pairing_code_chars)

# Raises a RateLimitErr if booped more than requests_per_day on average. The model is there is a "tank" of allowed
# requests. Each request depleats the tank by 1. each X amount of time a request is added to the tank (such that
# requests_per_window are added in window_seconds). the tank is capped at requests_per_window. If the tank is empty no
# more requests are allowed. Rate limited requests do not depleat the tank.
class RateLimiter:
    def __init__(self, name: str, requests_per_window: float, window_seconds: float) -> None:
        self.name = name
        self._spawn_time = window_seconds / requests_per_window
        self._full_tank = requests_per_window
        self._last_spawned_at = time.time()
        self._current_tank = self._full_tank

    # Does not use up a request, just raises if we are currently rate limiting
    def check(self) -> None:
        time_since_last_spawn = time.time() - self._last_spawned_at
        new_spawned = int(time_since_last_spawn / self._spawn_time)
        self._current_tank += new_spawned
        self._last_spawned_at += new_spawned * self._spawn_time
        if self._current_tank <= 0:
            raise RateLimitErr('rate limited due to too many ' + self.name)

    # Either raises or uses a request
    def boop(self) -> None:
        self.check()
        self._current_tank -= 1

class Player:
    def __init__(self, token: str, stable_code: str, last_active_at: float) -> None:
        assert token.startswith('PLYR-')
        self.token = token
        self.stable_code = stable_code
        # UNIX timestamp of the last time this session was active
        self.last_active_at = last_active_at
        self.pipe: Optional[PlayerRx] = None

    def serialize(self) -> dict:
        if self.pipe is not None:
            self.last_active_at = time.time()
        return {
            'token': self.token,
            'stable_code': self.stable_code,
            'last_active_at': self.last_active_at,
        }

    @staticmethod
    def deserialize(data: dict) -> 'Player':
        token = data.pop('token')
        assert isinstance(token, str)
        stable_code = data.pop('stable_code', None)
        if not isinstance(stable_code, str):
            stable_code = generate_stable_code()
        last_active_at = data.pop('last_active_at')
        assert isinstance(last_active_at, int) or isinstance(last_active_at, float)
        assert not data, 'data members left over after deserialization ' + repr(data)
        return Player(token, stable_code, last_active_at)

class Control:
    def __init__(self, token: str, last_active_at: float, player_token: str) -> None:
        assert token.startswith('CTRL-')
        assert player_token.startswith('PLYR-')
        self.token = token
        # UNIX timestamp of the last time this session was active
        self.last_active_at = last_active_at
        self.player_token = player_token
        self.pipe: Optional[ControlRx] = None

    def serialize(self) -> dict:
        if self.pipe is not None:
            self.last_active_at = time.time()
        return {
            'token': self.token,
            'last_active_at': self.last_active_at,
            'player': self.player_token,
        }

    @staticmethod
    def deserialize(data: dict) -> 'Control':
        token = data.pop('token')
        assert isinstance(token, str)
        last_active_at = data.pop('last_active_at')
        assert isinstance(last_active_at, int) or isinstance(last_active_at, float)
        player_token = data.pop('player')
        assert isinstance(player_token, str)
        assert not data, 'data members left over after deserialization ' + repr(data)
        return Control(token, last_active_at, player_token)

class Session:
    def __init__(self, model: 'Model', player_tx: Pipe, player: Player) -> None:
        self.model = model
        self.player_tx = player_tx
        self.control_txs: set[Pipe] = set()
        self.control_count = 0
        self.player = player
        self.pairing_code = ''
        self.state: dict = {'type': 'state'}
        self.awaiting_local_files_response: set[Pipe] = set()

    async def send_to_all_controls(self, message: dict) -> None:
        coroutines = []
        for tx in self.control_txs:
            coroutines.append(tx.send(message))
        await asyncio.gather(*coroutines)

    async def send_local_files_response(self, message: dict) -> None:
        coroutines = []
        for tx in self.awaiting_local_files_response:
            coroutines.append(tx.send(message))
        self.awaiting_local_files_response.clear()
        await asyncio.gather(*coroutines)

    def authorized_message(self, send_stable: bool) -> dict:
        assert self.pairing_code
        return {
            'type': 'authorized',
            'code': self.pairing_code,
            'stable': self.player.stable_code if send_stable else None,
            'commit': repo.cached_latest_commit,
        }

    async def set_pairing_code(self, pairing_code: str) -> None:
        self.pairing_code = pairing_code
        await asyncio.gather(
            self.player_tx.send(self.authorized_message(True)),
            self.send_to_all_controls(self.authorized_message(False))
        )

    async def add_control(self, tx: Pipe) -> None:
        self.control_txs.add(tx)
        await asyncio.gather(
            self.update_control_count(),
            tx.send(self.authorized_message(False))
        )
        await tx.send(self.state)

    async def remove_control(self, tx: Pipe, reason: str) -> None:
        self.control_txs.remove(tx)
        await tx.close(reason)
        await self.update_control_count()

    async def update_control_count(self):
        new_count = len(self.control_txs)
        if new_count != self.control_count:
            self.control_count = new_count
            await self.player_tx.send({
                'type': 'control_count',
                'count': self.control_count,
            })

    async def close(self, reason: str) -> None:
        await self.player_tx.close(reason)
        for tx in list(self.control_txs):
            await tx.close(reason)

class PlayerRx(Pipe):
    def __init__(self, session: Session, player: Player) -> None:
        super().__init__()
        self.session = session
        self.player = player
        self.player.pipe = self
        self.player.last_active_at = time.time()

    async def received(self, data) -> None:
        t = data.get('type')
        if t == 'state':
            self.session.state |= data
            await self.session.send_to_all_controls(data)
        elif t == 'local-files-response':
            await self.session.send_local_files_response(data)
        else:
            raise MessageErr('player message type invalid: ' + repr(data))

    async def closed(self, reason: str):
        self.session.model.sessions.pop(self.session.pairing_code)
        self.session.model.sessions.pop(self.session.player.stable_code)
        self.player.pipe = None
        self.player.last_active_at = time.time()
        await self.session.close(reason)

control_rx_message_types = set(['enqueue', 'move-media', 'drop-media', 'action', 'tv-volume', 'tv-power', 'local-files-request'])

class ControlRx(Pipe):
    def __init__(self, session: Session, tx: Pipe, control: Control) -> None:
        super().__init__()
        self.session = session
        self.tx = tx
        self.control = control
        self.control.pipe = self
        self.control.last_active_at = time.time()

    async def received(self, data) -> None:
        t = data.get('type')
        if t in control_rx_message_types:
            if t == 'local-files-request':
                self.session.awaiting_local_files_response.add(self.tx)
            await self.session.player_tx.send(data)
        else:
            raise MessageErr('control message type invalid: ' + repr(data))

    async def closed(self, reason: str):
        self.control.pipe = None
        self.control.last_active_at = time.time()
        await self.session.remove_control(self.tx, reason)

class Model:
    def __init__(self) -> None:
        self.controls: dict[str, Control] = {}
        self.players: dict[str, Player] = {}
        self.sessions: dict[str, Session] = {}
        self.connect_limit = RateLimiter('connections', 1000, 60)
        self.invalid_token_limit = RateLimiter('invalid tokens', 100, 60)
        self.register_limit = RateLimiter('new devices registered', 50, 60 * 60)
        self.invalid_pairing_code_limit = RateLimiter('invalid pairing codes', 10, 12 * 60 * 60)
        try:
            with open('db.json', 'r') as f:
                data = json.load(f)
            players = data.pop('players')
            controls = data.pop('controls')
            assert not data, 'loaded data has invalid properties: ' + repr(list(data.keys()))
            for item in players:
                player = Player.deserialize(item)
                self.players[player.token] = player
            for item in controls:
                control = Control.deserialize(item)
                self.controls[control.token] = control
        except FileNotFoundError:
            pass

    async def background_task(self) -> None:
        while True:
            await asyncio.sleep(60 * 60)
            self.save()

    def save(self) -> None:
        current_time = time.time()
        for player in list(self.players.values()):
            if player.pipe is None and current_time - player.last_active_at > device_expire_time:
                self.players.pop(player.token)
        for control in list(self.controls.values()):
            if control.pipe is None and current_time - control.last_active_at > device_expire_time:
                self.controls.pop(control.token)
        data = {
            'players': [player.serialize() for player in self.players.values()],
            'controls': [control.serialize() for control in self.controls.values()],
        }
        with open('db.json', 'w') as f:
            json.dump(data, f)

    def is_player_token(self, token: str) -> bool:
        if token.startswith('PLYR-'):
            return True
        elif token.startswith('CTRL-'):
            return False
        else:
            raise ConnectionErr('token did not start with a valid prefix')

    async def connect_player_socket(self, tx: Pipe, token: str) -> Pipe:
        self.connect_limit.boop()
        self.invalid_token_limit.check()
        if len(self.sessions) > 100:
            raise ConnectionErr('too many open connections')
        player = self.players.get(token)
        if player is None:
            self.invalid_token_limit.boop()
            raise ConnectionErr('player token not known')
        if player.pipe is not None:
            logging.warning(token + ' connected multiple times')
            await player.pipe.close('same player connected multiple times')
        player.last_active_at = time.time()
        session = Session(self, tx, player)
        player_rx = PlayerRx(session, player)
        pairing_code = generate_pairing_code()
        self.sessions[pairing_code] = session
        self.sessions[player.stable_code] = session
        await session.set_pairing_code(pairing_code)
        return player_rx

    async def connect_control_socket(self, tx: Pipe, token: str) -> Pipe:
        self.connect_limit.boop()
        self.invalid_token_limit.check()
        control = self.controls.get(token)
        if control is None:
            self.invalid_token_limit.boop()
            raise ConnectionErr('control token not known')
        if control.pipe is not None:
            logging.warning(token + ' connected multiple times')
            await control.pipe.close('connected from another tab')
        player = self.players.get(control.player_token)
        if player is None:
            logging.error('control has invalid player')
            raise ConnectionErr('control\'s player no longer valid')
        if player.pipe is None:
            raise ConnectionErr('TV is offline')
        session = player.pipe.session
        if len(session.control_txs) > 20:
            raise ConnectionErr('too many devices connected to session')
        control.last_active_at = time.time()
        control_rx = ControlRx(session, tx, control)
        await session.add_control(tx)
        return control_rx

    def register_control(self, pairing_code: str) -> str:
        if len(pairing_code) != pairing_code_len and len(pairing_code) != stable_code_len:
            raise ConnectionErr('pairing code wrong length')
        self.register_limit.boop()
        if len(pairing_code) < stable_code_len:
            self.invalid_pairing_code_limit.check()
        if len(self.controls) > 10000:
            raise ConnectionErr('too many controls')
        session = self.sessions.get(pairing_code)
        if session is None:
            if len(pairing_code) < stable_code_len:
                self.invalid_pairing_code_limit.boop()
            raise ConnectionErr('bad pairing code')
        token = 'CTRL-' + generate_token()
        self.controls[token] = Control(token, time.time(), session.player.token)
        return token

    def register_player(self) -> str:
        self.register_limit.boop()
        if len(self.players) > 1000:
            raise ConnectionErr('too many players')
        token = 'PLYR-' + generate_token()
        stable_code = generate_stable_code()
        self.players[token] = Player(token, stable_code, time.time())
        return token

    async def close_all_sessions(self) -> None:
        coroutines = [session.close('server shutting down') for session in self.sessions.values()]
        await asyncio.gather(*coroutines)
