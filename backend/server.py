from sanic import Sanic, Request, Websocket, text
import sanic
import os
import logging
import json
import asyncio
from typing import Optional
from model import Pipe, ConnectionErr, MessageErr, Model
import repo

max_message_size = 1000000

app = Sanic('tv200')
repo.init(app)

class WebsocketPipe(Pipe):
    def __init__(self, ws: Websocket) -> None:
        super().__init__()
        self.ws = ws
        self.inbound: Optional[Pipe] = None

    async def received(self, data: dict) -> None:
        await self.ws.send(json.dumps(data))

    async def closed(self, reason: str):
        await self.ws.close(reason=reason)
        if self.inbound is not None:
            await self.inbound.close(reason)

class BufferPipe(Pipe):
    def __init__(self) -> None:
        super().__init__()
        self.is_first = True
        self.response: Optional[sanic.HTTPResponse] = None

    async def received(self, data: dict) -> None:
        if self.response is not None:
            await self.response.send(
                ('  ' if self.is_first else ', ') +
                json.dumps(data) +
                '\n'
            )
            self.is_first = False

    async def closed(self, reason: str):
        if self.response is not None:
            await self.response.send(']\n', True)

def get_model() -> Model:
    return Sanic.get_app().ctx.model

@app.websocket("/websocket/<token:str>")
async def websocket(request: Request, ws: Websocket, token: str):
    outbound = WebsocketPipe(ws)
    try:
        is_player = get_model().is_player_token(token)
        inbound = await (
            get_model().connect_player_socket(outbound, token)
            if is_player else
            get_model().connect_control_socket(outbound, token)
        )
        outbound.inbound = inbound
        try:
            while True:
                # If we don't have a timeout improperly closed connections hang forever
                data = await ws.recv(timeout=20)
                if data:
                    try:
                        if len(data) > max_message_size:
                            raise MessageErr(
                                'message ' +
                                str(len(data)) +
                                ' bytes long, bigger than max allowed size ' +
                                str(max_message_size)
                            )
                        parsed = json.loads(str(data))
                        if not isinstance(parsed, dict):
                            raise MessageErr('message not a dict: ' + repr(parsed))
                        await inbound.send(parsed)
                    except (json.JSONDecodeError, MessageErr) as e:
                        message = 'invalid message: ' + str(e)
                        logging.error(message)
                        await outbound.send({
                            'type': 'error',
                            'message': message,
                        })
        except asyncio.CancelledError:
            await outbound.close(('player' if is_player else 'control') + ' closed')
            raise
    except ConnectionErr as e:
        await outbound.close(str(e))
        return text(str(e), status=e.status())
    except Exception as e:
        logging.error('error handling message: ' + str(e))
        message = 'internal server error'
        await outbound.close(message)
        return text(message, status=500)
    finally:
        await outbound.close('unknown reason for close')


@app.route('/API/oneshot/<token:str>', methods=["POST"])
async def http_oneshot(request: Request, token: str):
    inbound: Optional[Pipe] = None
    outbound: Optional[BufferPipe] = None
    try:
        timeout = float(request.args.get('timeout', 0))
        outbound = BufferPipe()
        response = await request.respond(content_type="application/json")
        outbound.response = response
        await response.send('[\n')
        inbound = await get_model().connect_control_socket(outbound, token)
        if not isinstance(request.json, list):
            raise MessageErr('request body is not a JSON list: ' + repr(request.json))
        for message in request.json:
            if not isinstance(message, dict):
                raise MessageErr('message not a dict: ' + repr(message))
            await inbound.send(message)
        if timeout:
            await asyncio.sleep(timeout)
    except Exception as e:
        if outbound is not None:
            await outbound.send({'type': 'error', 'message': str(e)})
    finally:
        if outbound is not None:
            await outbound.close('connection was HTTP oneshot')
        if inbound is not None:
            await inbound.close('connection was HTTP oneshot')

@app.route('/API/new_control_token/<pairing_code:str>', methods=["POST"])
async def new_control_token(request: Request, pairing_code: str):
    try:
        return text(get_model().register_control(pairing_code))
    except ConnectionErr as e:
        return text(str(e), status=e.status())

@app.route('/API/new_player_token', methods=["POST"])
async def new_player_token(request: Request):
    try:
        return text(get_model().register_player())
    except ConnectionErr as e:
        return text(str(e), status=e.status())

control_dir = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'control')
app.static("/", os.path.join(control_dir, 'index.html'), name='control_index')
app.static("/", control_dir, name='control_files')

@app.route('/lastCommit.js')
async def last_commit_js(request: Request):
    script = f'const lastCommit = "{repo.cached_latest_commit}";'
    return text(script, content_type='text/javascript')

@app.listener("before_server_start")
async def before_start(app):
    app.ctx.model = Model()
    app.add_task(app.ctx.model.background_task(), name='background_task')

@app.listener("before_server_stop")
async def before_stop(app):
    await app.cancel_task('background_task')
    await get_model().close_all_sessions()

@app.listener("after_server_stop")
def after_stop(app):
    get_model().save()

def run(debug: bool) -> None:
    app.run(
        port=5055,
        single_process=True,
        access_log=debug,
        debug=debug
    )
