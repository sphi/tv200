// If there are any services song.link supports that aren't listed here, please add them
// itmss://itunes.apple.com may also be convertable? Unclear
const musicServices = new Set([
  'music.amazon.com',
  'audiomack.com',
  'play.anghami.com',
  'deezer.com',
  'boomplay.com',
  'music.yandex.ru',
  'music.apple.com',
  'geo.music.apple.com',
  'play.napster.com',
  'soundcloud.com',
  'listen.tidal.com',
  'pandora.com',
  'youtube.com',
  'music.youtube.com',
  'open.spotify.com',
  'spotify.link',
]);

function musicServiceOf(url) {
  if (url.startsWith('http://') || url.startsWith('https://')) {
    hostname = new URL(url).hostname;
    while (hostname) {
      if (musicServices.has(hostname)) {
        return hostname;
      }
      hostname = hostname.split(/\.(.*)/s)[1];
    }
  }
  return null;
}

async function getUsableUrl(url) {
  const musicService = musicServiceOf(url);
  const preferredServices = await getConfigValue('preferred-music-services');

  if (musicService == 'open.spotify.com') {
    // Seems to pester us about using XDG open if we don't strip the si query param
    url = url.replace(/si=\w+/, '');
  }

  if (!musicServices.has(musicService) || preferredServices.includes(musicService)) {
    return url;
  } else {
    setStatus('Converting link...');
    try {
      const jsonResponse = await fetch(
        'https://api.song.link/v1-alpha.1/links?songIfSingle=true&url=' + encodeURIComponent(url)
      );
      const songlinkJson = await jsonResponse.json();
      let foundUrl = null;
      let foundIndex = preferredServices.length;
      for (key in songlinkJson.linksByPlatform) {
        const url = songlinkJson.linksByPlatform[key].url;
        const service = musicServiceOf(url);
        const index = preferredServices.indexOf(service);
        if (index >= 0 && index < foundIndex) {
          foundUrl = url;
          foundIndex = index;
        }
      }
      if (foundUrl) {
        return foundUrl;
      } else {
        showError('Could not find preferred link');
        return url;
      }
    } catch (err) {
      showError('Problem converting link: ' + err);
      return url;
    }
  }
}

function domainRegex(domain) {
  return new RegExp('(http(s)?://)?((^/)*\\.)?' + domain + '($|/)');
}
const titleRegex = new RegExp('<title[^>]*>(.*)</title[^>]*>');
const youtubeHtmlRegex = new RegExp('"title":"([^"]*)"');
const webyeetRegex = new RegExp('https://webyeet.com/.*/([^/]+)');
const fileRegex = new RegExp('file://.*/([^/]+)');
const youtubeRegex = domainRegex('youtube.com');
const shortYoutubeRegex = domainRegex('youtu.be');
async function getTitleForUrl(url) {
  let urlTitle = url.startsWith('https://') ? url.substring(8) : url;
  if (url.match(youtubeRegex) || url.match(shortYoutubeRegex)) {
    try {
      const metadataUrl = `https://www.youtube.com/oembed?url=${encodeURIComponent(url)}&format=json`;
      const response = await fetch(metadataUrl);
      const content = await response.text();
      metadata = JSON.parse(content);
      return metadata.title;
    } catch (err) {
      try {
        const title = await findRegexInHtmlFromUrl(url, youtubeHtmlRegex);
        // Title is inside JSON, so hopefully this will correctly handle escapes
        return JSON.parse('"' + title + '"');
      } catch (err2) {
        showError('Could not get title for Youtube link: ' + err.toString());
        showError('Could not get title for link using HTML: ' + err2.toString());
        return urlTitle;
      }
    }
  } else if (url.match(webyeetRegex)) {
    return decodeURI(url.match(webyeetRegex)[1]);
  } else if (url.match(fileRegex)) {
    return decodeURI(url.match(fileRegex)[1]);
  } else {
    try {
      return await findRegexInHtmlFromUrl(url, titleRegex);
    } catch (err) {
      console.error('Could not get title for link: ' + err.toString());
      return urlTitle;
    }
  }
}
async function findRegexInHtmlFromUrl(url, regex) {
  const response = await fetch(url);
  const contentType = response.headers.get('Content-Type');
  const contentLen = parseInt(response.headers.get('Content-Length'));
  const maxContentLen = 10 * 1024 * 1024;
  if (!response.ok) {
    throw 'code ' + request.status;
  } else if (!contentType.includes('text/html')) {
    throw 'not HTML';
  } else if (contentLen > maxContentLen) {
    throw 'too big (' + byteSizeToString(contentLen) + ')';
  } else {
    const content = await response.text();
    const match = content.match(regex)[1];
    var elem = document.createElement("textarea");
    elem.innerHTML = match;
    return elem.value;
  }
}
