/// ActiveMedia class
///   mediaId: string
///   activate() -> void
///   action(message) -> void
///   close() -> void

/// ActiveMedia object or null
let activeMedia = null;

let lastProgressTime = 0;
/// Properties are same as the `state` message
///   queue: array of media objects that are played in order. If queue is empty no media is playing. If queue is non-empty the 0th item is playing.
///   playing: `true` if playing, false if paused, ended, or not applicable
///   length: length of the media in seconds, or `null` if not applicable/unknown
///   progress: current progress through the media in seconds, or `null` if not applicable/unknown
///   speed: playback speed, 1.0 is normal
///   actions: list of supported actions
let mediaState = {};
let mediaDoneTimeout = null;
let checkForUpdatesWhenQueueEmpty = false;

class BrowserTabMedia {
  constructor(url, mediaId) {
    this.url = url;
    this.mediaId = mediaId;
    this.tabId = null;
    this.open = false;
  }

  activate() {
    this.open = true;
    chrome.runtime.sendMessage({
      type: 'open-tab',
      url: this.url,
    }, tabId => {
      if (typeof tabId === 'number') {
        this.tabId = tabId;
        if (!this.open) {
          this.close();
        }
      } else {
        console.error('tabId ', tabId, ' is not a number');
      }
    });
  }

  action(message) {
    if (this.tabId) {
      chrome.tabs.sendMessage(this.tabId, message);
    } else {
      showError('Media tab not open');
    }
  }

  close() {
    this.open = false;
    if (this.tabId !== null) {
      chrome.tabs.sendMessage(this.tabId, {
        type: 'prepare-close',
      });
      chrome.runtime.sendMessage({
        type: 'close-tab',
        tabId: this.tabId,
      });
      this.tabId = null;
    }
  }
}

class NativeMedia {
  constructor(url, mediaId) {
    this.url = url;
    this.mediaId = mediaId;
    this.open = false;
    this.hasSetVol = false;
    this.hasLoaded = false;
    this.vlcTrackCount = 0;
    this.subtitleTracks = [];
    this.currentAudioId = null;
    this.audioTracks = [];
    this.currentSubtitlesId = -1;
  }

  async activate() {
    if (this.open) {
      return;
    }
    this.open = true;
    this.hasLoaded = false;
    await sendSystemMessage({type: 'open-native', url: this.url});
    for (let i = 0; i < 200; i++) {
      await delay(100);
      if (await this.vlcFetch('status.json', true)) {
        await this.vlcFetch('status.xml?command=volume&val=256');
        await this.pollState();
        return;
      }
      if (i === 30) {
        showError('Waiting for VLC to start...');
      }
    }
    showError('VLC failed to start :(');
    await this.close();
  }

  async vlcFetch(path, supressError) {
    // vlc --intf qt --extraintf http --http-host localhost --http-port 9090 --http-password tv200 ~/Downloads/Sintel/Sintel.mp4
    // curl --header 'Authorization: Basic OnR2MjAw' 'http://localhost:9090/requests/status.json'
    const base64Credentials = btoa(':tv200');
    try {
      return await fetch(new Request('http://localhost:9090/requests/' + path, {
        method: 'GET',
        headers: { 'Authorization': `Basic ${base64Credentials}` }
      }));
    } catch (err) {
      if (!supressError) {
        showError('VLC error: ' + err.message);
      }
      return null;
    }
  }

  async updateState() {
    const response = await this.vlcFetch('status.json');
    if (!response) {
      return;
    }
    const parsed = await response.json();
    let sendTracks = false;
    if (this.hasLoaded && parsed.state == 'stopped') {
        advanceQueue();
        return
    } else if (!this.hasLoaded && parsed.state != 'stopped') {
      this.hasLoaded = true;
    }
    const vlcTrackCount = Object.keys(parsed.information?.category ?? {}).length;
    if (parsed.information && vlcTrackCount !== this.vlcTrackCount) {
      this.subtitleTracks = [{id: -1, name: false }];
      this.audioTracks = [];
      this.vlcTrackCount = vlcTrackCount;
      sendTracks = true;
      for (let key in parsed.information?.category ?? {}) {
        const stream = parsed.information.category[key];
        const id = Number(key.replace(/^[^\d]+(\d+).*$/, '$1'));
        let name = stream.Language ?? '';
        if (stream.Description) {
          name = stream.Description + (name ? ' (' + name + ')' : '');
        } else if (!name) {
          name = 'Track ' + id;
        }
        if (stream.Type == 'Subtitle') {
          this.subtitleTracks.push({id: id, name: name})
        } else if (stream.Type == 'Audio') {
          this.audioTracks.push({id: id, name: name})
          if (this.currentAudioId === null || id < this.currentAudioId) {
            this.currentAudioId = id;
          }
        }
      }
    }
    updateMediaState({
      playing: parsed.state === 'playing',
      length: parsed.length,
      progress: parsed.time,
      speed: parsed.rate,
      actions: ['toggle-play', 'seek', 'prev', 'next', 'select-track'],
      tracks: sendTracks ? this.getTracks() : undefined,
    });
  }

  getTracks() {
    const ret = [];
    if (this.subtitleTracks.length > 1) {
      for (const track of this.subtitleTracks) {
        ret.push({
          type: 'subtitles',
          name: track.name,
          selected: track.id === this.currentSubtitlesId,
        });
      }
    }
    if (this.audioTracks.length > 1) {
      for (const track of this.audioTracks) {
        ret.push({
          type: 'audio',
          name: track.name,
          selected: track.id === this.currentAudioId,
        });
      }
    }
    return ret;
  }

  async pollState() {
    if (this.open) {
      await this.updateState();
      if (!this.hasSetVol) {
        this.hasSetVol = true;
        await this.vlcFetch('status.json?command=volume&val=256');
      }
      setTimeout(() => this.pollState(), 5000);
    }
  }

  async action(message) {
    if (message.action === 'toggle-play') {
      await this.vlcFetch('status.json?command=pl_pause');
    } else if (message.action === 'next' || message.action === 'prev') {
      const delta = message.action === 'next' ? 10 : -10;
      const response = await this.vlcFetch('status.json');
      if (response) {
        const parsed = await response.json();
        await this.vlcFetch('status.json?command=seek&val=' + Math.round(parsed.time + delta));
      }
    } else if (message.action === 'seek') {
      await this.vlcFetch('status.json?command=seek&val=' + Math.round(message.time));
    } else if (message.action === 'select-track') {
      let found = false;
      if (message['track-type'] === 'subtitles') {
        for (const track of this.subtitleTracks) {
          if (track.name === message['track-name']) {
            await this.vlcFetch('status.json?command=subtitle_track&val=' + track.id);
            this.currentSubtitlesId = track.id;
            found = true;
            break;
          }
        }
      } else if (message['track-type'] === 'audio') {
        for (const track of this.audioTracks) {
          if (track.name === message['track-name']) {
            await this.vlcFetch('status.json?command=audio_track&val=' + track.id);
            this.currentAudioId = track.id;
            found = true;
            break;
          }
        }
      } else {
        console.error('Unsupported track type', message['track-type']);
        found = true;
      }
      if (!found) {
        console.error('Did not find ' + message['track-type'] + ' track named', message['track-name']);
      }
      updateMediaState({
        tracks: this.getTracks(),
      });
    } else {
      showError('Unsupported action ' + message.action);
    }
    await this.updateState();
  }

  async close() {
    if (this.open) {
      this.open = false;
      await sendSystemMessage({type: 'close-native'});
    }
  }
}

function fileExtOf(path) {
  const fileName = path.substring(path.lastIndexOf('/'));
  const index = fileName.lastIndexOf('.');
  if (index >= 0) {
    return fileName.substring(index + 1);
  } else {
    return '';
  }
}

// See https://en.wikipedia.org/wiki/Video_file_format
const videoExtsLowerCase = new Set(['mkv', 'flv', 'vob', 'ogv', 'ogg', 'avi', 'mts', 'm2ts', 'ts', 'mov', 'qt', 'wmv', 'yuv', 'asf', 'amv', 'mp4', 'm4p', 'm4v', 'mpg', 'mp2', 'mpeg', 'mpe', 'mpv', 'm2v', 'svi', '3gp', '3g2', 'mxf', 'roq', 'nsv', 'webm']);
function newMedia(url, id) {
  if (url.startsWith('vlc:')) {
    return new NativeMedia(url.substring(4), id);
  } else if (videoExtsLowerCase.has(fileExtOf(url).toLowerCase())) {
    return new NativeMedia(url, id);
  } else {
    return new BrowserTabMedia(url, id);
  }
}

function maybeCheckForUpdates() {
  getConfigValue('auto-updates').then(enabled => {
    if (enabled) {
      if (mediaState.queue.length) {
        checkForUpdatesWhenQueueEmpty = true;
      } else {
        sendSystemMessage({type: 'check-for-update'});
      }
    }
  }).catch(showError);
}

function advanceQueue() {
  if (mediaState.queue.length > 0) {
    mediaState.queue.splice(0, 1);
    updateMediaState({
      queue: mediaState.queue,
      length: null,
    });
  }
}

function updateMediaState(state) {
  // If the current media ID according to the new state's queue doesn't match the actual current
  // media ID
  if (state.queue !== undefined)
  {
    state.queue = [...state.queue]; // clone
    // First, close any active media that isn't front of the queue
    if (activeMedia) {
      if (!state.queue.length || state.queue[0].id !== activeMedia.mediaId) {
        activeMedia.close();
        activeMedia = null;
        state.playing = false;
        state.length = null;
        state.progress = null;
        state.speed = 1.0;
        state.actions = [];
        state.tracks = [];
      }
    }

    // Next, if there is no active media open the first item in the queue
    if (state.queue.length && !activeMedia) {
      activeMedia = newMedia(state.queue[0].url, state.queue[0].id);
      activeMedia.activate();
    }

    // If the queue is empty, make sure all tabs are closed
    if (!state.queue.length) {
      chrome.runtime.sendMessage({type: 'close-all'});
      state = {
        queue: [],
        playing: false,
        length: null,
        progress: null,
        speed: 1.0,
        actions: [],
      }
      if (checkForUpdatesWhenQueueEmpty) {
        sendSystemMessage({type: 'check-for-update'});
        checkForUpdatesWhenQueueEmpty = false;
      }
    }
  }
  let send = false;
  let message = {
    type: 'state',
  };
  Object.keys(state).forEach(key => {
    if (state[key] !== undefined && state[key] !== mediaState[key]) {
      send = true;
      mediaState[key] = state[key];
      message[key] = state[key];
    }
  });
  if (state.progress) {
    lastProgressTime = performance.now();
  }
  if (send) {
    sendMessage(message);
  }
  clearTimeout(mediaDoneTimeout);
  if (mediaState.progress &&
      mediaState.length &&
      mediaState.speed) {
    if (mediaState.playing) {
      mediaDoneTimeout = setTimeout(() => {
        if (mediaState.queue.length > 1) {
          advanceQueue();
        }
      }, (mediaState.length - mediaState.progress) * 1000 / mediaState.speed);
    }
    if (!mediaState.playing &&
        mediaState.progress > mediaState.length - 0.1) {
      advanceQueue();
    }
  }
  updateStatus();
}

function sendInitialMediaState() {
  const state = mediaState;
  mediaState = {}
  updateMediaState(state);
}

function getMediaState() {
  if (mediaState.progress !== null && mediaState.playing) {
    const currentTime = performance.now();
    const deltaMs = currentTime - lastProgressTime;
    mediaState.progress += deltaMs * mediaState.speed / 1000;
    lastProgressTime = currentTime;
  }
  mediaState.type = 'state'
  return mediaState;
}

function addMediaToQueue(media, after) {
  if (after === null) {
    if (mediaState.queue.length) {
      mediaState.queue[0] = media;
    } else {
      mediaState.queue.push(media);
    }
  } else {
    const index = mediaState.queue.findIndex(media => media.id == after);
    mediaState.queue.splice(index < 0 ? Infinity : index + 1, 0, media);
  }
}

function removeMediaFromQueue(mediaId) {
  const index = mediaState.queue.findIndex(media => media.id === mediaId);
  if (index < 0) {
    console.warn('could not find item to drop from queue with ID', mediaId);
    return null;
  } else {
    //console.log('dropping item at queue position', index, 'with ID', mediaId);
    return mediaState.queue.splice(index, 1)[0];
  }
}
