const qbittorrentAddr = 'http://localhost:5057'
let torrentShouldBeRunning = false;
let downloadingTorrents = [];
let torrentCompletedTime = null;
let torrentDownloadingStates = new Set(['allocating', 'downloading', 'metaDL', 'pausedDL', 'queuedDL', 'stalledDL', 'checkingDL', 'forcedDL']);
let torrentSeedingStates = new Set(['uploading', 'queuedUP', 'stalledUP', 'checkingUP', 'forcedUP']);
let pendingAddTorrent = false;
let updateTorrentStateTimeoutId = null;

// For testing
// https://webtorrent.io/free-torrents

function flashPage() {
  const root = document.getElementById('root');
  root.classList.add('background-flash');
  setTimeout(() => {
    root.classList.remove('background-flash');
  }, 50);
}

function byteSizeToString(bytes) {
  for (let i of ['B', 'kB', 'MB', 'GB', 'TB']) {
    if (bytes < 10) {
      return (Math.round(bytes * 10) / 10).toFixed(1) + i;
    } else if (bytes < 1024) {
      return Math.round(bytes) + i;
    }
    bytes /= 1024;
  }
  return Math.round(bytes) + 'PB';
}

async function torrentStop() {
  torrentShouldBeRunning = null;
  await sendSystemMessage({
    'type': 'set-torrent-running',
    'running': false,
  });
  torrentCompletedTime = null;
}

function setTorrentStatus(status) {
  const elem = document.getElementById('torrent-status');
  if (status === null) {
    elem.style.display = 'none';
  } else {
    elem.style.display = 'block';
    elem.textContent = status;
  }
}

async function torrentStart() {
  if (!torrentShouldBeRunning) {
    await sendSystemMessage({
      'type': 'set-torrent-running',
      'running': true,
    });
    setTorrentStatus('Torrent client starting...');
    document.getElementById('torrent-additional-info').textContent = '';
    torrentCompletedTime = null;
  }
  for (let i = 0; i < 50; i++) {
    try {
      if ((await fetch(qbittorrentAddr + '/api/v2/app/version')).ok) {
        break;
      }
    } catch {
    }
    await delay(100);
  }
  document.getElementById('torrents').style.display = 'block';
  torrentShouldBeRunning = true;
  await updateTorrentState();
}

function createTorrent(json) {
  const frag = document.importNode(document.getElementById('torrent-downloading').content, true);
  const ret = {
    hash: json.hash,
    titleElem: frag.getElementById('torrent-downloading-title'),
    amountElem: frag.getElementById('torrent-downloading-amount'),
    progressElem: frag.getElementById('torrent-downloading-progress'),
    rootElem: frag.firstElementChild,
  };
  document.getElementById('torrents').prepend(frag.firstElementChild);
  return ret;
}

function updateTorrent(torrent, json) {
  let visibleState = json.state;
  if (visibleState.endsWith('DL')) {
    visibleState = visibleState.slice(0, -2);
  }
  if (visibleState == 'downloading') {
    visibleState = '';
  }
  if (visibleState) {
    visibleState = ' (' + visibleState + ')';
  }
  const name = json.name ? json.name : json.hash;
  torrent.titleElem.textContent = name + visibleState;
  torrent.amountElem.textContent = byteSizeToString(json.downloaded) + '/' + byteSizeToString(json.total_size);
  torrent.progressElem.style.width = Math.round(json.progress * 100) + '%';
}

function removeTorrent(torrent) {
  torrent.rootElem.remove();
}

function updateTorrentUI(jsonList) {
  let globalUpload = 0;
  const oldDownloading = downloadingTorrents;
  downloadingTorrents = [];
  let additionalDownCount = 0;
  let seedingCount = 0;
  let otherCount = 0;
  for (const json of jsonList) {
    globalUpload += json.uploaded_session;
    if (torrentDownloadingStates.has(json.state) && downloadingTorrents.length < 3) {
      let torrent = null;
      for (let i = 0; i < oldDownloading.length; i++) {
        if (oldDownloading[i] !== null && oldDownloading[i].hash == json.hash) {
          torrent = oldDownloading[i];
          oldDownloading[i] = null;
          break;
        }
      }
      if (torrent === null) {
        torrent = createTorrent(json);
      }
      downloadingTorrents.push(torrent);
      updateTorrent(torrent, json);
    } else if (torrentSeedingStates.has(json.state)) {
      seedingCount++;
    } else if (torrentDownloadingStates.has(json.state)) {
      additionalDownCount++;
    } else {
      otherCount++;
    }
  }
  for (const torrent of oldDownloading) {
    if (torrent !== null) {
      removeTorrent(torrent);
    }
  }
  let info = '';
  if (additionalDownCount) {
    info += 'downloading ' + additionalDownCount + ' more, ';
  }
  if (seedingCount) {
    info += 'seeding ' + seedingCount + ', ';
  }
  if (otherCount) {
    info += otherCount + ' in an unkown state, ';
  }
  info += 'uploaded ' + byteSizeToString(globalUpload);
  info.charAt(0).toUpperCase() + info.slice(1);
  document.getElementById('torrent-additional-info').textContent = info;
  if (downloadingTorrents.length) {
    torrentCompletedTime = null;
  } else if (torrentCompletedTime == null) {
    torrentCompletedTime = performance.now() / 1000;
  }
  if (torrentShouldBeRunning && oldDownloading.length && !downloadingTorrents.length) {
    flashPage();
  }
  if (downloadingTorrents.length || !torrentShouldBeRunning) {
    setTorrentStatus(null);
  } else if (pendingAddTorrent) {
    setTorrentStatus('Adding torrent...');
  } else {
    setTorrentStatus('Downloads complete');
  }
  updateStatus();
}

async function updateTorrentState() {
  clearTimeout(updateTorrentStateTimeoutId);
  updateTorrentStateTimeoutId = null;
  try {
    const response = await fetch(qbittorrentAddr + '/api/v2/torrents/info');
    if (!response.ok) {
      throw Error('Fetching torrents did not succeed: HTTP code ' + response.status);
    }
    if (torrentShouldBeRunning === false) {
      showError('Torrent client should not be running, but is');
      torrentShouldBeRunning = true;
    }
    const torrents = await response.json();
    updateTorrentUI(torrents);
    const timeSinceTorrentCompleted = torrentCompletedTime ? (performance.now() / 1000) - torrentCompletedTime : 0;
    if (timeSinceTorrentCompleted > 60 * 60) {
      showError('Torrents have been seeding long enough');
      await torrentStop();
    }
  } catch (e) {
    if (!torrentShouldBeRunning && e == 'TypeError: Failed to fetch') {
      // Expected
      torrentShouldBeRunning = false;
      updateTorrentUI([]);
      document.getElementById('torrents').style.display = 'none';
      return;
    }
    showError('Updating torrents: ' + e);
    await torrentStop();
  }
  document.getElementById('torrents').style.display = 'block';
  updateTorrentStateTimeoutId = setTimeout(updateTorrentState, 1000);
}

async function downloadTorrent(url) {
  pendingAddTorrent = true;
  try {
    if (torrentShouldBeRunning !== true) {
      await torrentStart();
    }
    const formData = new FormData();
    formData.append('urls', url);
    const response = await fetch(qbittorrentAddr + '/api/v2/torrents/add', {
      method: 'POST',
      body: formData,
    });
    if (!response.ok) {
      throw Error('Adding torrent did not succeed: HTTP code ' + response.status);
    }
    await updateTorrentState();
  } catch (e) {
    showError('Adding torrent: ' + e);
  } finally {
    pendingAddTorrent = false;
  }
}

updateTorrentState();
