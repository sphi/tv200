let socket = null;

function sendMessage(message) {
  if (socket) {
    socket.send(JSON.stringify(message));
  }
}

async function getToken() {
  const result = await chrome.storage.local.get('token');
  token = result.token;
  if (token) {
    return token;
  } else {
    showError('Requesting new token from server...');
    const backendUrl = await getConfigValue('backend-url');
    try {
      const response = await fetch(
        backendUrl + '/API/new_player_token',
        {method: 'POST'},
      );
      const text = await response.text();
      if (response.ok && text.startsWith('PLYR-')) {
        await chrome.storage.local.set({token: text});
        return text;
      } else {
        setStatus('Server error');
        const message = 'Failed to get token: ' + text;
        showError(message);
        throw Error(message);
      }
    } catch (err) {
      setStatus('Server offline');
      const message = 'Could not connect to server: ' + err;
      showError(message);
      throw Error(message);
    }
  }
}

function resetToken() {
  updateMediaState({queue: []});
  chrome.storage.local.set({token: null}, () => {
    initSocket();
  });
  const errorsElem = document.getElementById('errors');
  while (errorsElem.firstChild) {
    errorsElem.removeChild(errorsElem.firstChild);
  }
  showError('Player reset');
}

async function initSocket() {
  document.getElementById('peer-id').textContent = '...';
  setStatus('Initializing...');
  const token = await getToken();
  if (socket) {
    socket.close();
    socket = null;
  }
  let backendUrl = await getConfigValue('backend-url');
  backendUrl = backendUrl.replace('https://', 'wss://');
  backendUrl = backendUrl.replace('http://', 'ws://');
  const url = backendUrl + '/websocket/' + token;
  const localSocket = new WebSocket(url);
  localSocket.onclose = e => {
    if (socket && socket !== localSocket) {
      return;
    }
    document.getElementById('peer-id').textContent = '...';
    updateConnCount(0);
    if (navigator.onLine) {
      if (!socket && e.code == 1000) {
        // We got no messages, but exited normally (this means the server rejected the connection)
        resetToken();
      } else {
        // We had at least one message
        if (e.reason) {
          showError('Disconnected, ' + e.reason);
        }
        // Perhaps the server shut down due to an update? If auto-updates are enabled this will git pull locally
        // and restart us if it gets anything new (once the queue is empty)
        maybeCheckForUpdates();
        (async () => {
          for (let i = 3; i > 0; i--) {
            setStatus('Retrying in ' + i + '...');
            await delay(1000);
          }
          initSocket();
        })();
      }
    }
  };
  localSocket.onmessage = e => {
    try {
      if (!socket) {
        // First message
        updateStatus();
        socket = localSocket;
        sendInitialMediaState();
      }
      const message = JSON.parse(e.data);
      handleMessageFromBackend(message).catch(err => {
        showError('Error handling message: ' + err);
      });
    } catch (err) {
      showError('Error receiving message: ' + err);
    }
  };
}

addEventListener('offline', () => {
  showError('Lost network');
  setStatus('Offline :(');
});

addEventListener('online', () => {
  showError('Back online :)');
  if (!socket) {
    initSocket();
  }
});
