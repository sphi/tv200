const errorTimeout = 7000;
let connectedDeviceCount = 0;
let stableCode = '??';

function delay(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function showError(message) {
  console.error(message);
  const elem = document.getElementById('errors');
  const p = document.createElement('p');
  p.textContent = message;
  elem.appendChild(p);
  setTimeout(() => {
    try {
      elem.removeChild(p);
    } catch {}
  }, errorTimeout);
}

if (typeof chrome === 'undefined' || !chrome.runtime) {
  showError('Home page not connected to extension');
}

function setStatus(status) {
  document.getElementById('status').textContent = status;
}

function updateStatus() {
  if (mediaState.queue.length) {
    if (mediaState.queue[0].name) {
      setStatus('Playing ' + mediaState.queue[0].name);
    } else {
      setStatus('Loading...');
    }
  } else if (pendingAddTorrent || downloadingTorrents.length) {
    setStatus('Torrenting...');
  } else if (connectedDeviceCount === 0) {
    setStatus('Waiting');
  } else {
    setStatus('Ready');
  }
}

function updateConnCount(count) {
  connectedDeviceCount = count;
  const elem = document.getElementById('connected-devices');
  if (connectedDeviceCount === 0) {
    elem.textContent = 'no connected devices';
  } else if (connectedDeviceCount === 1) {
    elem.textContent = '1 connected device';
  } else {
    elem.textContent = connectedDeviceCount + ' connected devices';
  }
  updateStatus();
}

// Listen for messages from the content and background scripts
chrome.runtime.onMessage.addListener(
  (message, sender, _sendResponse) => {
    //console.log('Home got message:', message);
    if (message.type === 'spoof-input-event') {
      // Generate a fake input event for the new tab so it can make videos fullscreen
      sendSystemMessage({type: 'spoof-input-event'});
    } else if (message.type === 'state') {
      updateMediaState(message);
    } else if (message.type === 'ready') {
      // Ignore
    } else {
      console.error('Unknown message type: ' + JSON.stringify(message));
    }
  }
);

async function sendSystemMessageGetRespnse(message) {
  try {
    const response = await fetch(new Request(
      'http://localhost:5056/message', {
      method: 'POST',
      headers: {
        'Accept': 'text/plain',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(message)
    }));
    const text = await response.text();
    if (response.status >= 300) {
      showError('System error: ' + text);
      return null
    } else {
      return text ? text : null;
    }
  } catch (err) {
    showError('Error making system request: ' + err);
    return null;
  }
}

async function sendSystemMessage(message) {
  const text = await sendSystemMessageGetRespnse(message);
  if (text) {
    showError('System: ' + text);
  }
}

let cachedConfig = null;
let configPromise;
function reloadConfig() {
  cachedConfig = null;
  configPromise = (async () => {
    try {
      const text = await sendSystemMessageGetRespnse({ type: 'config-request' });
      const response = JSON.parse(text);
      if (response && response.type === 'config-response' && response.config) {
        cachedConfig = response.config;
        for (const warning of response.warnings) {
          showError(warning);
        }
      } else {
        showError('Invalid config response returned by system: ' + text);
        // Try to load the default config without the help of the system component
        // Note we can't load config.json from the project root directly like this as it's outside of the extension directory
        const defaultConfigUrl = '../default-config.json';
        const response = await fetch(defaultConfigUrl)
        cachedConfig = await response.json();
      }
    } catch (err) {
      showError('Problem loading config: ' + err);
    }
    configPromise = null;
  })();
}

async function getConfigValue(key) {
  if (configPromise !== null) {
    await configPromise;
  }
  if (!cachedConfig) {
    throw Error('Cannot get config value ' + JSON.stringify(key) + ' due to problem loading config');
  }
  const result = cachedConfig[key];
  if (result === undefined) {
    throw Error('Tried to get invalid configuration option ' + JSON.stringify(key));
  } else {
    return result;
  }
}

// Called from connection.js when we get a message from the backend (generally originating from a control device)
async function handleMessageFromBackend(message) {
  if (message.type === 'control_count') {
    // Displays number of control devices currently connected
    updateConnCount(message.count);
  } else if (message.type == 'authorized') {
    document.getElementById('peer-id').textContent = message.code;
    stableCode = message.stable;
  } else if (message.type === 'error') {
    showError(message.message);
  } else if (message.type === 'enqueue') {
    sendSystemMessage({type: 'tv-power', state: true});
    if (message.url.startsWith('magnet:')) {
      if (await getConfigValue('torrenting')) {
        await downloadTorrent(message.url);
      } else {
        setStatus('Torrenting not configured')
      }
      return;
    }
    const usableUrl = await getUsableUrl(message.url);
    //console.log('Usable URL: ' + usableUrl);
    addMediaToQueue({
      url: usableUrl,
      id: message.id,
      name: null,
    }, message.after);
    updateMediaState({queue: mediaState.queue});
    const title = await getTitleForUrl(usableUrl);
    for (let i = 0; i < mediaState.queue.length; i++) {
      if (mediaState.queue[i].id === message.id) {
        mediaState.queue[i].name = title;
      }
    }
    updateMediaState({queue: mediaState.queue});
  } else if (message.type === 'move-media') {
    const media = removeMediaFromQueue(message.id);
    if (media) {
      addMediaToQueue(media, message.after);
    }
    updateMediaState({queue: mediaState.queue});
  } else if (message.type === 'drop-media') {
    for (let i = 0; i < message.ids.length; i++) {
      removeMediaFromQueue(message.ids[i]);
    }
    updateMediaState({queue: mediaState.queue});
  } else if (message.type === 'action') {
    if (activeMedia) {
      activeMedia.action(message);
    } else {
      showError('Nothing is playing');
    }
  } else if (message.type === 'tv-power' || message.type === 'tv-volume') {
    sendSystemMessage(message);
  } else if (message.type == 'local-files-request') {
    const text = await sendSystemMessageGetRespnse(message);
    const response = JSON.parse(text);
    if (response.type != 'local-files-response') {
      throw Error('Invalid local files response: ' + text);
    }
    sendMessage(response);
  } else {
    showError('Message has invalid type: ' + JSON.stringify(message))
  }
}

function setControlUrl(withStableCode) {
  getConfigValue('backend-url').then(url => {
    const host = url.split('://').pop();
    document.getElementById('control-url').textContent = (
      host +
      (withStableCode ? '/?code=' + stableCode : '')
    );
  });
}

pendingKeyCommand = false;
pendingKeyCommandTimeout = null;
document.addEventListener('keydown', event => {
  if (event.key === 'Escape') {
    showError('Now press');
    showError('- R to reset state (force re-pairing and change stable code)');
    showError('- S to show stable pairing code (persistent across restarts)');
    showError('- C to reload configuration');
    showError('- Q to quit to desktop');
    clearTimeout(pendingKeyCommandTimeout);
    pendingKeyCommandTimeout = setTimeout(() => {
      pendingKeyCommand = false;
    }, errorTimeout);
    pendingKeyCommand = true;
  } else if (pendingKeyCommand) {
    clearTimeout(pendingKeyCommandTimeout);
    pendingKeyCommand = false;
    if (event.key === 'r') {
      resetToken();
      setControlUrl(false);
    } else if (event.key === 'q') {
      showError('Quitting...');
      chrome.runtime.sendMessage({
        type: 'quit',
      });
    } else if (event.key === 'c') {
      showError('Reloading config...');
      reloadConfig();
    } else if (event.key === 's') {
      setControlUrl(true);
      setTimeout(() => setControlUrl(false), 30000);
    } else {
      showError('Unknown command');
    }
  }
}, true);

reloadConfig();
setControlUrl(false);
updateMediaState({queue: []});
updateConnCount(0);
initSocket();
