const homeUrl = 'home/home.html';
const homeRegex = new RegExp('(moz-extension|chrome-extension)://[\\w-]+/' + homeUrl.replace('.', '\\.'));
const reloadExtUrl = 'http://reload.extension/';

/// Closes all tabs except ones in the given list of tab IDs, and the extenson management page
function closeAllExcept(keepOpenIds) {
  chrome.tabs.query({}, tabs => {
    for (let i = 0; i < tabs.length; i++) {
      if (!keepOpenIds.includes(tabs[i].id) &&
          tabs[i].url !== 'chrome://extensions/') {
        chrome.tabs.remove(tabs[i].id);
      }
    }
  });
}

/// Calls the given callback with the Tab of the home tab, creates the home tab if needed
function withHomeTab(func) {
  chrome.tabs.query({}, tabs => {
    home = null;
    for (let i = 0; i < tabs.length; i++) {
      if (tabs[i].url.match(homeRegex)) {
        if (home) {
          chrome.tabs.remove(tabs[i].id);
        } else {
          home = tabs[i];
        }
      }
    }
    if (home) {
      func(home);
    } else {
      console.log('Did not find home tab, creating it');
      // If there is no home tab, close all currently open tabs
      chrome.tabs.create({
        url: homeUrl,
      }, tab => {
        closeAllExcept([tab.id]);
        func(tab);
      });
    }
  });
}

function fixUrl(url) {
  if (url.startsWith('https:') || url.startsWith('http:') || url.startsWith('file:')) {
    return url;
  } else {
    return 'https://' + url;
  }
}

chrome.runtime.onMessage.addListener(
  (message, sender, sendResponse) => {
    if (message.type === 'ready') {
      chrome.tabs.update(
        sender.tab.id,
        {
          active: true,
        }
      );
      withHomeTab(home => {
        // Closing tabs a second time seems to help clean up pesky pages like YouTube Music that claim there's unsaved changes
        closeAllExcept([home.id, sender.tab.id]);
      });
    } else if (message.type === 'open-tab') {
      // Create new tab (not active) loading the new media
      chrome.tabs.create({
        url: fixUrl(message.url),
        active: false,
      }).then(tab => {
        sendResponse(tab.id);
      });
      return true; // Keep message channel open for async response
    } else if (message.type === 'close-tab') {
      // Close the requested tab
      chrome.tabs.remove(message.tabId);
      // Switch to home tab (unclear why this doesn't happen automatically)
      chrome.tabs.update(
        sender.tab.id,
        {
          active: true,
        }
      );
    } else if (message.type === 'close-all') {
      // Close all tabs except home
      closeAllExcept([sender.tab.id]);
    } else if (message.type === 'quit') {
      closeAllExcept([]);
    } else if (message.type === 'state') {
      // ignore
    } else if (message.type === 'spoof-input-event') {
      // ignore
    } else {
      console.error('Message sent to service worker has invalid type ' + JSON.stringify(message));
    }
  }
);

// When the browser is first started it's pointed at reloadExtUrl, which causes us to reload the extension
chrome.tabs.query({}, tabs => {
  let isReloading = false;
  for (let i = 0; i < tabs.length; i++) {
    if (tabs[i].url === reloadExtUrl) {
      isReloading = true;
      // We need to create an empty tab so that we can remove the reloadExtUrl tab without it closing the browser
      // note that we can't use the home page for this, since that will be automatically closed when we reload
      chrome.tabs.create({})
        .then(() => chrome.tabs.remove(tabs[i].id))
        .then(() => chrome.runtime.reload());
    }
  }
  if (!isReloading) {
    // Open the home tab if needed
    withHomeTab(() => {});
  }
});
