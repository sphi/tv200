let fadeDiv = null;
let manager = null;

function removeElementsExcept(elem, toKeep) {
  let i = 0;
  while (i < elem.children.length) {
    if (toKeep.includes(elem.children[i])) {
      i++;
    } else {
      elem.children[i].remove();
    }
  }
}

function waitFor(selector, callback) {
  const waitTime = 30;
  const interval = 0.2;
  const maxAttempts = waitTime / interval;
  let attempts = 0;
  function iteration() {
    const elem = document.querySelector(selector);
    if (elem) {
      callback(elem);
    } else if (attempts > maxAttempts) {
      console.warn('Could not find ' + selector + ' element in page');
    } else {
      attempts += 1;
      setTimeout(iteration, interval * 1000);
    }
  }
  iteration();
}

function clickOn(selector) {
  const elem = document.querySelector(selector);
  if (elem) {
    elem.click();
    return true;
  } else {
    console.error('Could not find element ' + selector);
    return false;
  }
}

function sendState(message) {
  message.type = 'state';
  chrome.runtime.sendMessage(message);
}

function trackVideoState(video) {
  let timeout = null;
  function sendProgress() {
    clearTimeout(timeout);
    const message = {
      playing: !video.paused,
      length: video.duration ? video.duration : null,
      progress: video.currentTime,
    };
    sendState(message);
    if (video.playing) {
      timeout = setTimeout(sendProgress, 10000);
    }
  }
  let actions = ['toggle-play', 'seek', 'select-track'];
  sendState({
    playing: !video.paused,
    length: video.duration ? video.duration : null,
    progress: video.currentTime,
    speed: video.playbackRate,
    actions: actions,
  });
  if (video.playing) {
    timeout = setTimeout(sendProgress, 1000);
  }
  video.addEventListener('play', sendProgress);
  video.addEventListener('pause', sendProgress);
  video.addEventListener('seeked', sendProgress);
  video.addEventListener('durationchange', () => {
    sendState({length: video.duration});
  });
  video.addEventListener('ratechange', () => {
    sendState({speed: video.playbackRate});
  });
}

class GenericVideo {
  constructor() {
    this.video = null;
    this.updateTracks(null);
    if (this.isYoutube() && !this.isYoutubeMobile()) {
      // hacky but it works, switch to mobile youtube and change the setting
      const url = new URL(window.location.href);
      url.hostname = url.hostname.replace('www.youtube', 'm.youtube');
      url.searchParams.append('app', 'm');
      url.searchParams.append('persist_app', '1');
      window.location.href = url.toString();
      // to switch to mobile youtube:
      // http://m.youtube.com/?persist_app=1&app=m
      // to go back to desktop youtube:
      // https://www.youtube.com/?app=desktop&persist_app=1
    }
    if (this.isYoutubeMobile()) {
      waitFor('.ytp-large-play-button', playButton => {
        playButton.click();
        setTimeout(() => {
          this.initVideo();
        }, 200);
      });
    } else {
      this.initVideo();
    }
  }

  initVideo() {
    waitFor('video', videoElem => {
      this.video = videoElem;
      trackVideoState(this.video);
      this.video.play();
      this.video.style.cursor = 'none';

      // Make full screen if not youtube (youtube is handled below because it doesn't need the video element)
      if (!this.isYoutube()) {
        this.video.controls = true;
        onSpoofedInputEvent(() => {
          this.video.requestFullscreen();
        });
      }
    });

    if (this.isYoutubeMobile()) {
      waitFor('.icon-button.fullscreen-icon', fullscreenButton => {
        document.querySelector('.player-controls-background').style.cursor = 'none';
        onSpoofedInputEvent(() => {
          fullscreenButton.click();
        });
      });
    } else if (this.isYoutube()) {
      waitFor('.ytd-player .ytp-fullscreen-button', fullscreenButton => {
        onSpoofedInputEvent(() => {
          fullscreenButton.click();
        });
      });
    }
  }

  isYoutube() {
    return window.location.hostname.endsWith('youtube.com');
  }

  isYoutubeMobile() {
    return this.isYoutube() && window.location.hostname.startsWith('m.youtube');
  }

  setShowControls(show) {
    if (this.isYoutubeMobile()) {
      const isShown = document.querySelector('.new-controls').classList.contains('fadein');
      if (isShown != show) {
        document.querySelector('.player-controls-background').click();
      }
    }
  }

  togglePlay() {
    if (this.video) {
      if (this.video.paused || this.video.ended) {
        this.video.play();
        this.setShowControls(false);
      } else {
        this.video.pause();
        this.setShowControls(true);
      }
    }
  }

  seekTo(time) {
    if (this.video) {
      this.video.currentTime = time;
      this.setShowControls(true);
    }
  }

  skipNext() {
    if (this.video) {
      this.video.currentTime += 10;
      this.setShowControls(true);
    }
  }

  skipPrev() {
    if (this.video) {
      this.video.currentTime -= 10;
      this.setShowControls(true);
    }
  }

  // Just sends track if message is null
  updateTracks(message) {
    if (!this.isYoutubeMobile()) {
      sendState({tracks: []});
      return;
    }
    waitFor('.ytmClosedCaptioningButtonButton', subtitlesButton => {
      let pressed = subtitlesButton?.ariaPressed == 'true';
      if (pressed ?? null === null) {
        sendState({tracks: []});
      }
      if (message && message['track-type'] == 'subtitles') {
        if (!!message['track-name'] !== pressed) {
          subtitlesButton.click();
          pressed = !pressed;
        }
      }
      sendState({tracks: [
        {type: 'subtitles', name: false, selected: !pressed},
        {type: 'subtitles', name: true, selected: pressed},
      ]});
    });
  }
}

class YtMusic {
  constructor() {
    this.video = null;
    this.updateTracks(null);
    waitFor('video', elem => {
      this.video = elem;
      trackVideoState(this.video);
    });
    if (window.location.pathname === '/playlist') {
      clickOn('.ytmusic-section-list-renderer .title a');
    }
  }

  togglePlay() {
    if (this.video) {
      if (this.video.paused || this.video.ended) {
        this.video.play();
      } else {
        this.video.pause();
      }
    }
  }

  seekTo(time) {
    this.video.currentTime = time;
  }

  skipNext() {
    clickOn('.next-button');
  }

  skipPrev() {
    clickOn('.previous-button');
  }

  // Just sends tracks if message is null
  updateTracks(message) {
    const elems = document.querySelectorAll('.tab-content');
    setTimeout(() => this.updateTracks(null), elems.length === 0 ? 1000 : 10000);
    let lyricsElem = null;
    let upNextElem = null;
    for (let i = 0; i < elems.length; i++) {
      if (elems[i].textContent.toLowerCase().includes('lyrics')) {
        lyricsElem = elems[i];
      } else if (elems[i].textContent.toLowerCase().includes('up next')) {
        upNextElem = elems[i];
      }
    }
    if (message && message['track-type'] == 'lyrics') {
      if (lyricsElem && message['track-name']) {
        lyricsElem.click();
      } else if (upNextElem && !message['track-name']) {
        upNextElem.click();
      }
    }
    if (!lyricsElem || lyricsElem.parentElement.ariaDisabled === 'true') {
      sendState({tracks: []});
      return;
    }
    const lyricsShown = lyricsElem ? lyricsElem.parentElement.classList.contains('iron-selected') : false;
    const tracks = [
      {type: 'lyrics', name: false, selected: !lyricsShown},
      {type: 'lyrics', name: true, selected: lyricsShown},
    ]
    sendState({tracks: tracks});
  }
}

class Pandora {
  constructor() {
  }

  togglePlay() {
    clickOn('.PlayButton');
  }

  skipNext() {
    clickOn('.SkipButton');
  }
}

class Spotify {
  constructor() {
    waitFor('[data-testid=play-button]', playBtn => {
      playBtn.click();
    });
  }

  togglePlay() {
    clickOn('[data-testid=control-button-playpause]');
  }

  skipNext() {
    clickOn('[data-testid=control-button-skip-forward]');
  }

  skipPrev() {
    clickOn('[data-testid=control-button-skip-back]');
  }
}

function handleMessage(message) {
  if (message.type === 'action') {
    if (message.action === 'toggle-play') {
      manager.togglePlay();
    } else if (message.action === 'next') {
      manager.skipNext();
    } else if (message.action === 'prev') {
      manager.skipPrev();
    } else if (message.action === 'seek') {
      manager.seekTo(message.time);
    } else if (message.action === 'select-track') {
      manager.updateTracks(message);
    } else {
      console.error('Unknown action ' + message.action);
    }
  } else if (message.type === 'prepare-close') {
    // Removing all elements prevents the annoying close tab confirmations
    removeElementsExcept(document.documentElement, [fadeDiv]);
  } else {
    console.error('Unknown message type: ' + JSON.stringify(message));
  }
}

function initManager() {
  const hostname = window.location.hostname;
  if (hostname.endsWith('pandora.com')) {
    manager = new Pandora();
  } else if (hostname.endsWith('spotify.com')) {
    manager = new Spotify();
  } else if (hostname.endsWith('music.youtube.com')) {
    manager = new YtMusic();
  } else {
    manager = new GenericVideo();
  }
}

chrome.runtime.onMessage.addListener(
  (message, _sender, _sendResponse) => {
    handleMessage(message);
  }
);

function onSpoofedInputEvent(callback) {
  console.log('Requesting spoofed input event..');
  window.addEventListener("keydown", e => {
    console.log('Responding to input event ' + e.code);
    callback();
  }, {once: true});
  chrome.runtime.sendMessage({
    type: 'spoof-input-event',
  });
}

window.addEventListener('load', () => {
  chrome.runtime.sendMessage({
    type: 'ready',
  });
});

// Can't hurt, prevents some of the close tab confirmations (but has no effect against the addEventListener() ones)
window.onbeforeunload = null;

initManager();
