# tv200
A browser-based open source smart TV platform

## Features
If you have a computer or RaspberryPi 4+ connected to your TV, tv200 lets you control it from an arbitrary number of phones and computers. You send a link and the TV shows or plays it. It supports the following features:
- Change TV volume and turn TV on and off from your control devices. TV is automatically turned on when you send a link
- Automatic conversion of music links from all common platforms to your preferred platform
- A multi-platform, multi-user editable queue
- Video files (both local and streamed) can be played in VLC, and you can play/seek/etc within VLC
- You can cast local files from your control devices
- You can browse and play media downloaded to the TV player device
- You can download torrents simply by sending a magnet link
- CLI client, ideal for scripting

## Supported services
Play/pause/skip work on these platforms:
- Youtube (you can also seek within a video)
- Youtube Music
- Spotify
- Pandora
- Arbitrary sites with HTML5 videos (though support is not as good)
- More can be added quite easily, PRs welcome

## Quickstart
1. Install dependencies:
  - Linux running an X11 desktop
  - `Chromium`/`Google Chrome`
  - `git`
  - Python3 package `sanic`
  - `xdotool` (helps fullscreening videos)
  - `vlc` (optional)
  - `libcec` (optional, devices other than Raspberry Pi generally don't have hardware support)
2. Clone this repository onto the computer that's connected to your TV
3. Close any open Chrome windows
4. Run `system/main.py`
5. Go to [tv.phie.me](https://tv.phie.me) from another device
6. Type in the code that's displayed on your TV
7. Copy the URL of a public video you want to watch
8. Click on the wide play button in the middle of the screen and paste the URL to play your video

`system/main.py` launches the browser+player extension, and runs a local webserver for various features not available directly from a browser. `scripts/pi4-system-setup.sh` is what I use to install dependencies and make `system/main.py` run on boot on my Raspberry Pi 4. You'll probably need to tweak it or just use it as a reference if anything about your setup is different from mine.

See [documentation.md](documentation.md) for more details and internal documentation.

## Configuration
By default many features are disabled. They can be enabled with a `config.json` file in the repo's root directory. Values absent from your config fall back to those in [player/default-config.json](player/default-config.json). [winter-config.json](winter-config.json) is my personal config with all features enabled. I symlink it to `config.json`, and you're welcome to do the same.

You can reload configuration (respected by both system and player) by, on the player's keyboard, pressing `ESC` and then `C`.

### `auto-updates`
`true`/`false`. If to run git pull and restart the browser when an update is detected from the backend. Does NOT change your branch, just pulls.

### `backend-url`
The URL of where backend/main.py is running. You are welcome but not required to use my deployment. If you're running backend/main.py locally, use `http://localhost:5055`.

### `preferred-music-services`
An array of domains. When a user sends a link to a known service not on the list, the TV attempts to convert the link to a service on this list with [Songlink](https://odesli.co/). This is useful if, for example, you have a Spotify subscription but your guest shares a song from Apple Music. Earlier enteries are preferred over later enteries. See [player/home/linkLoading.js](player/home/linkLoading.js) for the full list of services that can be converted to/from.

### `power-mode`
How TV power is toggled, options are `null` and `"cec"`.

For CEC libcec must be installed and device must have hardware support (Raspberry Pis support CEC, most other devices don't).

### `volume-mode`
How volume is changed, options are `null`, `"cec"` and `"pulse"`. Same caveats for CEC as above. Pulse reqires the `pactl` command.

### `file-browsing-root`
`null` to disable file browsing, else a directory users can browse through from the control app's Downloads menu. The contents of files are not sent, only file names and directory structure. When a user clicks on a file its path is sent back to the player to play it. Note that the entire subdirectory tree is traversed and serialized every time a user browses files, so a directory with too many things in it might bog the system down.

### `torrenting`
`true`/`false`. If to automatically start qBittorrent and download torrents when magnet links are sent to the TV".

### `torrenting-netns`
The network namespace to run qBittorrent within. Useful for using a VPN only with qBittorrent. Has no effect if torrenting is disabled. Set to null to not run within a network namespace. Passwordless sudo is required to use a network namespace, but qBittorrent is not run as root.

## Troubleshooting
The easiest way to fix most problems is to restart the TV's computer.

To reset the TV's state and force all devices to re-pair with a new code press `Escape` on the TV device and then press `R`.

## Torrenting
The `pi4-system-setup.sh` script sets up a number of things to enable torrenting. Notably it:
- Creates a network namespace
- Prompts the user for their Mullvad account
- Sets up a VPN connection (which is only used inside the network namespace, not by the browser or rest of the system)
- Sets up localhost port forwarding so the browser can talk to programs running in the network namespace
- Configures qBittorrent

The network namespace stuff is only required if you want your torrent client to use a VPN and the rest of the system to not be on the VPN. Otherwise, all you have to do is configure qBittorrent like my setup script does (either by editing the config file or in the UI) and you're ready to go.

If you need more fine-grained controll over qBittorrent, when it's running you can send `http://localhost:5057` to the TV the same way you would a video, and use a keyboard/mouse connected to the TV device to mess with it.

## Setting up a dev environment
You currently need a chrome-based browser to run the player extension (because Firefox doesn't support manifest v3). The extension manages all tabs in that browser, so you'll want a different browser or browser profile to run the control web app in.

You might want to symlink [dev-config.json](dev-config.json) to `config.json` to use sensible defaults for local development. `ln -s "$PWD/dev-config.json" config.json`.

Running `system/main.py` is the best way to start both the system component and Chrome while developing.

`backend/main.py` is the server that is generally accessed over the internet. When you run it it will give you an IP/port (`http://127.0.0.1:5055`). Put this into your control browser's URL bar to open the local instance of the control web app.

It's generally best to run both the `system` and the `backend` server's with the `--dev` flag when developing.
