# tv200 Android app
This app is just a wrapper around a webview running the control web app, with a few small system integrations.

## Release process
1. Bump version name and versin code in `android/app/build.gradle.kts`
1. Bump androidAppVersionCode in `control/index.js`
1. In Android Studio `Build` -> `Generate Signed App Bundle/APK` -> `APK` -> Set the signing key and build
1. App should now be in `./android/app/release/app-release.apk`
1. Rename to `tv200-1.X.apk`
1. Add, commit and push the version number change
1. `git tag android-1.X && git push origin tag android-1.X`
1. [Create a new release in Codeberg](https://codeberg.org/sphi/tv200/releases/new)
1. Put in tag name and `Android App 1.X` as title
1. Upload APK
1. Click publish
