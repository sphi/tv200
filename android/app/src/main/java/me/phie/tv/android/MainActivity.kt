package me.phie.tv.android

import android.content.ClipboardManager
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.webkit.JavascriptInterface
import android.webkit.WebChromeClient
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import org.json.JSONObject
import me.phie.tv.android.BuildConfig

val TAG = "tv200"
// Local dev, don't ask me where this IP comes from
// NOTE: you also have to change android:usesCleartextTraffic to "true" in AndroidManifest.xml
//val BACKEND_URL = "http://10.0.2.2:5055"
val BACKEND_URL = "https://tv.phie.me";

class MainActivity : AppCompatActivity() {
    var webView: WebView? = null
    var queuedJs: String? = "";

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        webView = WebView(this)
        webView!!.settings.javaScriptEnabled = true
        webView!!.settings.domStorageEnabled = true
        // I don't know why this is needed, but without it injecting js into the page doesn't work
        webView!!.webChromeClient = object : WebChromeClient() {}
        webView!!.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                Log.d(TAG, "Page finished loading")
                val js = (queuedJs ?: "") + """
                    setMediaInputShown = (shown) => {
                        if (shown) {
                            playMedia(Android.getClipboard());
                        }
                    }
                """
                queuedJs = null
                webView!!.loadUrl("javascript:(() => {$js})()")
            }

            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                if (request?.url?.toString()?.startsWith(BACKEND_URL) == true) {
                    return false
                } else {
                    val intent = Intent(Intent.ACTION_VIEW, request?.url)
                    view!!.context.startActivity(intent)
                    return true
                }
            }
        }
        webView!!.addJavascriptInterface(this, "Android")
        setContentView(webView)

        if (savedInstanceState == null) {
            Log.d(TAG, "Loading webview...")
            webView!!.loadUrl(BACKEND_URL)
        } else {
            webView!!.restoreState(savedInstanceState)
            Log.d(TAG, "State restored")
        }

        if (intent != null) {
            handleIntent(intent)
        }
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        handleIntent(intent)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        webView!!.saveState(outState)
        Log.d(TAG, "State saved")
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP ) {
            handleVolChange(1)
            return true
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            handleVolChange(-1)
            return true
        } else {
            return super.onKeyDown(keyCode, event)
        }
    }

    @JavascriptInterface
    fun getClipboard(): String? {
        val clipBoardManager = getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
        return clipBoardManager.primaryClip?.getItemAt(0)?.text?.toString()
    }

    @JavascriptInterface
    fun getVersionCode(): Int {
        return BuildConfig.VERSION_CODE
    }

    fun runJsInPage(js: String) {
        if (queuedJs == null) {
            Log.d(TAG, "Running js...")
            webView!!.loadUrl("javascript:(() => {$js;})()")
        } else {
            Log.d(TAG, "Queueing js...")
            queuedJs += "$js;"
        }
    }

    fun handleIntent(intent: Intent) {
        if (intent.action == Intent.ACTION_SEND) {
            if (intent.type == "text/plain") {
                var str = intent.getStringExtra(Intent.EXTRA_TEXT) ?: ""
                var escapedString = JSONObject.quote(str)
                runJsInPage("addToQueue = true; playMedia($escapedString)")
                Toast.makeText(this, "Playing $escapedString", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun handleVolChange(change: Int) {
        val button_id = if (change > 0) "vol-up-button" else "vol-down-button"
        runJsInPage("""
            onVolumeClicked($change);
            document.getElementById('$button_id').classList.add('fake-active');
            setTimeout(() => {
                document.getElementById('$button_id').classList.remove('fake-active');
            }, 20);
        """.trimIndent())

    }
}
