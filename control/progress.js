let setProgress;
{
let lastProgress = 0;
let lastProgressTimestamp = 0;
let progressDurationMs = 0;
let progressIsMoving = false;
const progressBarElem = document.getElementById('progress-bar');
const progressParentElem = progressBarElem.parentElement;
// The amount the progress is allowed to be wrong due to rounding errors in the element sizes
const progressSizeErrorPixels = 1.5;
// The amount the progress is allowed to be wrong due to timing issues
const progressTimeErrorMs = 100;

document.getElementById('progress-hitbox').addEventListener('mouseup', ev => {
  const position = ev.offsetX / progressBarElem.parentElement.offsetWidth;
  const requestSeconds = (position * progressDurationMs) / 1000;
  requestSeekTo(requestSeconds);
});

const progressAtTime = (timestamp) => {
  if (progressIsMoving) {
    let progressAtTime = lastProgress;
    if (progressDurationMs) {
      const msSinceLast = timestamp - lastProgressTimestamp;
      progressAtTime += msSinceLast / progressDurationMs;
    }
    return Math.max(Math.min(progressAtTime, 1), 0);
  } else {
    return Math.max(Math.min(lastProgress, 1), 0);
  }
}

const updateProgress = () => {
  const pageCurrrentProgress = progressBarElem.offsetWidth / progressParentElem.offsetWidth;
  const possibleSizeError = progressSizeErrorPixels / progressParentElem.offsetWidth;
  const possibleTimeError = progressTimeErrorMs / (progressDurationMs ? progressDurationMs : 1);
  const possibleError = Math.max(possibleSizeError, possibleTimeError);
  const currentTimestamp = performance.now();
  const modelCurrentProgress = progressAtTime(currentTimestamp);
  const delta = Math.abs(pageCurrrentProgress - modelCurrentProgress);
  if (delta > possibleError) {
    // We are in the wrong place, move to the right place
    const transitionMs = 100;
    const target = progressAtTime(currentTimestamp + transitionMs);
    progressBarElem.style.transition = 'width ' + transitionMs + 'ms linear';
    progressBarElem.style.width = target * 100 + '%';
    //console.log('progress at ' + pageCurrrentProgress + ' with ' + delta + ' error, moving to ' + target);
  } else if (progressIsMoving && modelCurrentProgress < 1) {
    // We are in the right place, but progress is moving so we need to keep moving with it
    const transitionMs = progressDurationMs * (1 - modelCurrentProgress);
    progressBarElem.style.transition = 'width ' + transitionMs + 'ms linear';
    progressBarElem.style.width = '100%';
    //console.log('progress correctly at ' + pageCurrrentProgress + ' with ' + delta + ' error, moving normally');
  } else {
    progressBarElem.style.transition = 'width 0s';
    progressBarElem.style.width = pageCurrrentProgress * 100 + '%';
    //console.log('progress stopped at ' + pageCurrrentProgress + ' with ' + delta + ' error');
  }
}

progressBarElem.addEventListener('transitionend', () => {
  updateProgress();
});

setProgress = (currentSeconds, totalSeconds, isMoving) => {
  lastProgress = currentSeconds / totalSeconds;
  lastProgressTimestamp = performance.now();
  progressDurationMs = totalSeconds * 1000;
  progressIsMoving = isMoving;
  updateProgress();
}
}
