let fileBrowser = null;

function onDownloadsClicked() {
  onGrabsDismissed();
  const frag = document.importNode(document.getElementById('file-browser').content, true);
  fileBrowser = {
    searchHeader: frag.getElementById('file-browser-search-header'),
    searchInput: frag.getElementById('file-browser-search-input'),
    searchClear: frag.getElementById('file-browser-search-clear'),
    dirHeader: frag.getElementById('file-browser-dir-header'),
    pathElem: frag.getElementById('file-browser-current-path'),
    itemList: frag.getElementById('file-browser-item-list'),
    currentPath: [],
  }
  requestDownloadsContent();
  showPopup(frag, 'Close');
}

function requestDownloadsContent() {
  fileBrowser.response = null;
  fileBrowser.responseTime = null;
  refreshFileBrowserElem();
  fileBrowser.requestTime = performance.now();
  sendMessage({
    'type': 'local-files-request'
  });
}

function handleFileBrowserResponse(message) {
  if (fileBrowser) {
    fileBrowser.response = message;
    fileBrowser.responseTime = performance.now();
    console.log('Took ' + (fileBrowser.responseTime - fileBrowser.requestTime) + 'ms to request local files');
    refreshFileBrowserElem();
  }
}

function getItemFromDirContents(contents, name) {
  for (let item of contents) {
    if (item.name == name) {
      return item
    }
  }
  return null;
}

function setTextWithWbrs(elem, text) {
  while (elem.lastChild) {
    elem.removeChild(elem.lastChild);
  }
  let wbrNext = false;
  for (span of text.match(/[a-zA-Z0-9\s]+|./g)) {
    if (wbrNext) {
      elem.appendChild(document.createElement('wbr'));
    }
    wbrNext = true;
    elem.appendChild(document.createTextNode(span));
  }
}

function setFileBrowserItemList(itemList) {
  fileBrowser.itemList.replaceChildren();
  for (item of itemList) {
    const frag = document.importNode(document.getElementById('file-browser-item').content, true);
    const itemType = item.type;
    const itemName = item.name;
    const itemPath = item.path ?? [...fileBrowser.currentPath, itemName].join('/');
    const iconElem = frag.getElementById('file-browser-item-icon');
    if (itemType == 'dir') {
      iconElem.textContent = '📁';
    } else if (pathIsVideo(itemName)) {
      iconElem.textContent = '📼';
    } else {
      iconElem.textContent = '📄';
    }
    setTextWithWbrs(frag.getElementById('file-browser-item-name'), itemName);
    if (itemType == 'dir') {
      frag.children[0].onclick = () => {
        fileBrowser.currentPath = itemPath.split('/');
        fileBrowser.searchInput.value = '';
        refreshFileBrowserElem();
      }
    } else {
      const mediaPath = 'file://' + fileBrowser.response.root + '/' + itemPath;
      frag.children[0].onclick = () => {
        playMedia(mediaPath);
        onGrabsDismissed();
      }
    }
    fileBrowser.itemList.appendChild(frag);
  }
}

function refreshFileBrowserElem() {
  let itemList = [];
  let showSearch = false;

  if (!fileBrowser.response) {
    fileBrowser.pathElem.textContent = 'Loading...';
  } else if (fileBrowser.response.error) {
    fileBrowser.pathElem.textContent = 'Error';
    showError(fileBrowser.response.error);
  } else if (fileBrowser.searchInput.value !== '' || fileBrowser.currentPath.length === 0) {
    showSearch = true;
    fileBrowser.searchClear.style.display = fileBrowser.searchInput.value === '' ? 'none' : '';
    if (fileBrowser.searchInput.value === '') {
      itemList = fileBrowser.response.contents;
    } else {
      const matchingVideos = [];
      const matchingOther = [];
      const appendMatching = (keywords, path, items) => {
        for (const item of items) {
          if (itemList.length > 30) {
            return;
          }
          if (keywords.every(keyword => item.name.toLowerCase().includes(keyword))) {
            const list = pathIsVideo(item.name) ? matchingVideos : matchingOther;
            list.push({
              type: item.type,
              name: item.name,
              path: path + item.name,
            });
          }
          if (item.type == 'dir') {
            appendMatching(keywords, path + item.name + '/', item.contents);
          }
        }
      }
      const keywords = fileBrowser.searchInput.value.split(' ').filter(x => x !== '').map(x => x.toLowerCase());
      appendMatching(keywords, '', fileBrowser.response.contents);
      itemList = [...matchingVideos, ...matchingOther];
    }
  } else {
    // Show whole path:
    // const baseName = fileBrowser.response.root.split('/').pop();
    // const visiblePath = fileBrowser.currentPath.length ? `${baseName}/${fileBrowser.currentPath.join('/')}` : baseName;

    // Just show directory name
    const visiblePath = fileBrowser.currentPath.slice(-1)[0] || fileBrowser.response.root.split('/').pop();

    fileBrowser.pathElem.textContent = visiblePath;

    itemList = fileBrowser.response.contents;
    for (let name of fileBrowser.currentPath) {
      const item = getItemFromDirContents(itemList, name);
      if (item && item.type == 'dir') {
        itemList = item.contents;
      } else {
        itemList = []
      }
    }
  }

  fileBrowser.searchHeader.style.display = showSearch ? '' : 'none';
  fileBrowser.dirHeader.style.display = showSearch ? 'none' : '';
  setFileBrowserItemList(itemList);

  if (showSearch) {
    fileBrowser.searchInput.focus();
  }
}

function onUpDirectoryClicked() {
  fileBrowser.currentPath.pop();
  refreshFileBrowserElem();
}

function onRefreshFilesClicked() {
  fileBrowser.response = null;
  refreshFileBrowserElem();
  sendMessage({
    'type': 'local-files-request'
  });
}

function onClearFilesSearch() {
  fileBrowser.searchInput.value = '';
  refreshFileBrowserElem();
}

function handleFileBrowserResponse(message) {
  if (fileBrowser) {
    fileBrowser.response = message;
    refreshFileBrowserElem();
  }
}
