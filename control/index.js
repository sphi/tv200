const androidAppVersionCode = 2;
let mediaState = {};
let isConnected = false;
let addToQueue = true; // else play now
let lastInputEventTouch = false;

if ('serviceWorker' in navigator) {
  navigator.serviceWorker
    .register('/serviceWorker.js')
    //.then(() => { console.log('Service Worker Registered'); });
}

function setStatus(status) {
  if (status === null) {
    if (mediaState.queue.length) {
      if (mediaState.playing) {
        status = 'Playing ' + mediaState.queue[0].name;
      } else {
        status = 'Paused ' + mediaState.queue[0].name;
      }
    } else {
      status = 'Ready';
    }
  }
  //console.log('Status set to', status);
  document.getElementById('status').textContent = status;
}

function showPopup(elem, dismissLabel) {
  // TODO support nested popups and support multiple errors in the error popup
  document.getElementById('popup-content').appendChild(elem);
  document.getElementById('popup-container').style.display = 'flex';
  document.getElementById('dismiss-div').style.display = 'block';
  document.getElementById('popup-dismiss-button').textContent = dismissLabel || 'OK';
}

function showError(message) {
  let elem;
  console.error(message);
  elem = document.createElement('p');
  elem.textContent = message;
  showPopup(elem);
}

function onGrabsDismissed() {
  // Clear children so they are not present if the popup is used again
  const content = document.getElementById('popup-content');
  while (content.lastChild) {
    content.removeChild(content.lastChild);
  }
  document.getElementById('popup-container').style.display = 'none'
  const dropdowns = document.getElementsByClassName('dropdown');
  for (let i = 0; i < dropdowns.length; i++) {
    dropdowns[i].classList.remove('active');
  }
  document.getElementById('dismiss-div').style.display = 'none';
  fileBrowser = null;
  trackSelector = null;
}

function flashPage() {
  const root = document.getElementById('root');
  root.classList.add('background-flash');
  setTimeout(() => {
    root.classList.remove('background-flash');
  }, 20);
}

function playMedia(media) {
  //console.log('Playing media ' + media);
  if (!media) {
    setStatus('No media');
    return null;
  }
  const id = generateId();
  const prevId = (addToQueue && mediaState.queue.length) ? mediaState.queue[mediaState.queue.length - 1].id : null;
  sendMessage({
    type: 'enqueue',
    url: media,
    id: id,
    after: prevId,
  });
  flashPage();
  setStatus('Playing');
  return id;
}

async function tryPlayPastedMedia() {
  try {
    const clipboardContents = await navigator.clipboard.read();
    for (const item of clipboardContents) {
      for (const mimeType of item.types) {
        if (mimeType === "text/plain") {
          const blob = await item.getType("text/plain");
          const blobText = await blob.text();
          console.log(blobText);
          playMedia(blobText);
          setMediaInputShown(false);
        }
      }
    }
  } catch (err) {
    // Let the user paste manually
    console.error(err);
  }
}

function setMediaInputShown(shown) {
  const mediaInput = document.getElementById('media-input');
  const mediaButtons = document.getElementById('media-buttons');
  mediaInput.style.display = shown ? 'block' : 'none';
  mediaButtons.style.display = shown ? 'none' : 'flex';
  if (shown) {
    mediaInput.focus();
    mediaInput.select();
    if (lastInputEventTouch) {
      // The navigator.clipboard.read() method give a better UI only if Ctrl+V isn't available
      tryPlayPastedMedia();
    }
  } else {
    mediaInput.value = '';
    mediaInput.blur();
  }
}

{
  const mediaInput = document.getElementById('media-input');
  mediaInput.addEventListener('focusout', () => {
    setMediaInputShown(false);
  });
  // We don't listen to the 'paste' event because modern android keyboard sends clipboard data
  // without pasting
  mediaInput.addEventListener('input', ev => {
    const media = ev.target.value;
    if (!media || media.length < 2) {
      showError('Please paste a link');
    } else {
      playMedia(ev.target.value);
      setMediaInputShown(false);
    }
  });
}

function onAboutClicked() {
  onGrabsDismissed();
  const frag = document.importNode(document.getElementById('about').content, true);
  frag.getElementById('last-commit').textContent = lastCommit.substring(0, 12);
  showPopup(frag);
}

function onPlayNowClicked() {
  addToQueue = false;
  setMediaInputShown(true);
}

function onAddToQueueClicked() {
  addToQueue = true;
  setMediaInputShown(true);
}

function requestSeekTo(seconds) {
  sendMessage({
    type: 'action',
    action: 'seek',
    time: seconds,
  });
}

function onPlayPauseClicked() {
  sendMessage({
    type: 'action',
    action: 'toggle-play',
  });
}

function onSkipPrevClicked() {
  sendMessage({
    type: 'action',
    action: 'prev',
  });
}

function onSkipNextClicked() {
  sendMessage({
    type: 'action',
    action: 'next',
  });
}

function updateConnectedState(connected) {
  isConnected = connected;
  function setDisplay(className, display) {
    const collection = document.getElementsByClassName(className);
    for (let i = 0; i < collection.length; i++) {
      collection[i].style.display = display ? '' : 'none';
    }
  }
  setDisplay("only-when-connected", connected);
  setDisplay("only-when-disconnected", !connected);
  setDisplay("only-when-autoconnect-possible", !connected && hasToken());
  setDisplay("only-when-autoconnect-impossible", !connected && !hasToken());
  document.getElementById('content').style.display = connected ? 'flex' : 'none';
  const inputElem = document.getElementById('peer-id-input');
  inputElem.disabled = connected;
  if (connected) {
    inputElem.blur();
  }
  document.documentElement.classList.toggle('is-connected', connected);
}

let resettingState = false;
function resetState() {
  onGrabsDismissed();
  if (hasToken()) {
    setStatus('Not connected');
  } else {
    setStatus('Enter pairing code to connect (not case sensitive)');
  }
  //console.log('Resetting state...');
  disconnectExplicitly();
  socket = null;
  updateConnectedState(false);
  setMediaInputShown(false);
  mediaState = {
    queue: [],
    playing: false,
    length: null,
    progress: null,
    speed: 1.0,
    actions: [],
  };
}

function focusIdInput() {
  const elem = document.getElementById('peer-id-input');
  elem.setSelectionRange(elem.value.length, elem.value.length);
  elem.focus();
}

function createQueueElem() {
  const template = document.getElementById('queue-item');
  return template.content.cloneNode(true);
}

function initMoveMediaButton(button, item, itemIndex, excludeIndex, moveToAfterId) {
  if (itemIndex == 0 || itemIndex == excludeIndex) {
    button.style.display = 'none';
  } else {
    button.style.display = '';
    button.onclick = () => {
      sendMessage({
        type: 'move-media',
        id: item.id,
        after: moveToAfterId,
      });
      onGrabsDismissed();
    }
  }
}

function initQueueElem(elem, index, queue) {
  const item = queue[index];
  getDescendantById(elem, 'queue-item-name').textContent = item.name ? item.name : item.url;
  // NOTE: we need to use .onclick instead of .addEventListener() so that stale listeners don't
  // stick around when an item is reused
  getDescendantById(elem, 'queue-item-remove').onclick = () => {
    sendMessage({
      type: 'drop-media',
      ids: [item.id],
    });
  };
  initMoveMediaButton(getDescendantById(elem, 'queue-item-play-now'), item, index, 0, null);
  initMoveMediaButton(getDescendantById(elem, 'queue-item-play-next'), item, index, 1, queue[0].id);
  initMoveMediaButton(getDescendantById(elem, 'queue-item-play-last'), item, index, queue.length - 1, queue[queue.length - 1].id);
  getDescendantById(elem, 'queue-item-copy-link').onclick = () => {
    navigator.clipboard.writeText(item.url);
    setStatus('Link copied to clipboard');
    onGrabsDismissed();
  };
}

function updateQueue(queue) {
  const queueElem = document.getElementById('queue-div');
  while (queueElem.children.length < queue.length) {
    queueElem.appendChild(createQueueElem())
  }
  while (queueElem.children.length > queue.length) {
    queueElem.removeChild(queueElem.lastChild);
  }
  for (let i = 0; i < queue.length; i++) {
    initQueueElem(queueElem.children[i], i, queue);
  }
  document.getElementById('cast-file-play-now').style.display = queue.length ? 'none' : 'inline';
  document.getElementById('cast-file-add-to-queue').style.display = queue.length ? 'inline' : 'none';
  document.getElementById('add-to-queue-media-button').style.display = queue.length ? 'inline' : 'none';
}

function handleState(message) {
  Object.keys(message).forEach(key => {
    mediaState[key] = message[key];
  });
  if (hasAny(message, ['queue'])) {
    updateQueue(message.queue);
  }
  if (hasAny(message, ['queue', 'playing'])) {
    setStatus(null);
  }
  if (hasAny(message, ['playing', 'length', 'progress', 'speed'])) {
    if (mediaState.length !== null && mediaState.progress !== null) {
      setProgress(
        mediaState.progress / mediaState.speed,
        mediaState.length / mediaState.speed,
        mediaState.playing,
      );
    } else {
      setProgress(0, 1, false);
    }
  }
  if (message.tracks !== undefined) {
    tracksUpdated();
  }
}

function handleAuthorized(message) {
  setStatus('Connected');
  updateConnectedState(true);
  tracksUpdated();
  document.getElementById('peer-id-input').value = message.code;
  // lastCommit comes from the lastCommit.js script. This check verifies we're not on a stale version of the web app
  if (!lastCommit) {
    showError('Failed to load last commit');
  } else if (message.commit !== lastCommit) {
    location.reload(true);
  }
}

function handleMessage(message) {
  if (message.type === 'authorized') {
    handleAuthorized(message)
  } else if (message.type === 'state') {
    handleState(message);
  } else if (message.type === 'local-files-response') {
    handleFileBrowserResponse(message);
  } else if (message.type === 'error') {
    showError(message.message);
  } else {
    showError('message has unknown type: ' + JSON.stringify(message));
  }
}

function onDropdownClicked(button) {
  // The positioning code handles dropdowns inside scroll boxes, where the browser doesn't work right by default
  const dropdown = button.parentElement;
  const menu = dropdown.querySelector('.dropdown-menu');
  document.getElementById('dismiss-div').style.display = 'block';
  dropdown.classList.toggle('active', true);
  const buttonRect = button.getBoundingClientRect();
  const dropdownRect = menu.getBoundingClientRect();
  menu.style.top = `${buttonRect.bottom + window.scrollY}px`;
  menu.style.left = `${buttonRect.left + buttonRect.width - dropdownRect.width + window.scrollX}px`;
}

function onConnectClicked() {
  const pairingId = document.getElementById('peer-id-input').value.toUpperCase();
  tryPair(pairingId);
}

function onDisconnectClicked() {
  resetState();
}

function onForgetPlayerClicked() {
  clearToken();
  resetState();
}

// Usually the desired action is to turn the TV off, however if the button is pressed twice
// in a small amount of time turn the TV on
let powerButtonTimeout = null;
function onPowerClicked() {
  if (powerButtonTimeout === null) {
    setStatus('TV off');
    powerButtonTimeout = setTimeout(() => {
      powerButtonTimeout = null;
    }, 10000);
    sendMessage({
      type: 'tv-power',
      state: false,
    });
  } else {
    setStatus('TV on');
    clearTimeout(powerButtonTimeout);
    powerButtonTimeout = null;
    sendMessage({
      type: 'tv-power',
      state: true,
    });
  }
}

function onVolumeClicked(steps) {
  sendMessage({
    type: 'tv-volume',
    steps: steps,
  });
}

function androidAppOutOfDate() {
  if (!window.Android) return false;
  // Assume apps without version code are version 2
  const actualVersionCode = window.Android.getVersionCode ? window.Android.getVersionCode() : 2;
  return actualVersionCode < androidAppVersionCode;
}

try {
  document.addEventListener('touchend', () => lastInputEventTouch = true);

  resetState();

  const url = new URL(window.location);
  const codeParam = url.searchParams.get('code');
  if (codeParam) {
    url.searchParams.delete('code');
    history.replaceState(null, '', url);
    tryPair(codeParam);
  } else {
    tryAutoconnect();
  }

  if (navigator.userAgent.toLowerCase().indexOf("android") > -1 && !window.Android) {
    // We are running on and Android device but not in the Android app
    document.getElementById('android-app-link').style.display = 'block';
  } else if (androidAppOutOfDate()) {
    // We're in the android app, and the app version is old
    document.getElementById('android-update-info').style.display = 'block';
  }
} catch (err) {
  showError('Error initializing: ' + err)
}
