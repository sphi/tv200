import WebYeetFileServer from 'https://webyeet.com/client.mjs';
let uploadedFiles = []; // mutable
let fileServer = null;

async function createFileServer() {
  if (fileServer === null) {
    setStatus('Starting webyeet...');
    fileServer = await (WebYeetFileServer.fromNewSession("https://webyeet.com", uploadedFiles));
    await fileServer.connect();
    setStatus(null);
  }
}

async function uploadFiles(files) {
  // uploadedFiles.length = 0
  // TODO: drop files that are no longer in the queue
  uploadedFiles.push(...files);
  await createFileServer();
  for (let file of files) {
    const url = encodeURI(`${fileServer.yeeturl.origin}/${fileServer.token}/${file.name}`);
    addToQueue = true;
    playMedia(url);
  }
}

function installDragListeners() {
  let dragCount = 0;

  const root = document.getElementById('root');
  const overlay = document.getElementById('cast-file-overlay');

  function updateState() {
    if (isConnected && dragCount > 0) {
      overlay.style.display = 'flex';
    } else {
      dragCount = 0;
      overlay.style.display = 'none';
    }
  }

  root.addEventListener('dragenter', ev => {
    ev.preventDefault();
    dragCount++;
    updateState();
  }, false);

  root.addEventListener('dragleave', ev => {
    ev.preventDefault();
    dragCount--;
    updateState();
  }, false);

  root.addEventListener("dragover", ev => {
    ev.preventDefault();
  }, false);

  root.addEventListener('drop', ev => {
    ev.preventDefault();
    dragCount = 0;
    updateState();
    if (isConnected) {
      uploadFiles(ev.dataTransfer.files);
    }
  }, false);
}

installDragListeners();

document.getElementById('cast-file-input').addEventListener('change', async ev => {
  onGrabsDismissed();
  await uploadFiles(ev.target.files);
});
