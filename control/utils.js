const idLength = 8;

function hasAny(obj, props) {
  for (let i = 0; i < props.length; i++) {
    if (obj[props[i]] !== undefined) {
      return true;
    }
  }
  return false;
}

function generateId() {
  result = '';
  for (let i = 0; i < idLength; i++) {
    const num = Math.floor(Math.random() * 52);
    if (num >= 26) {
      result += String.fromCharCode(num - 26 + 97);
    } else {
      result += String.fromCharCode(num + 65);
    }
  }
  return result;
}

// Recursively searches element tree, returns first element that matches or null
function getDescendant(elem, pred) {
  if (pred(elem)) {
    return elem;
  }
  for (let i = 0; i < elem.children.length; i++) {
    const result = getDescendant(elem.children[i], pred);
    if (result !== null) {
      return result;
    }
  }
  return null;
}

function getDescendantById(elem, id) {
  return getDescendant(elem, e => e.id === id);
}

function instantiateTemplate(name) {
  return document.getElementById(name).content.children[0].cloneNode(true);
}

// listElem should initially contain a single template, which will be instantiated for each array elem
function updateListContents(listElem, dataSource, updateElem) {
  const template = listElem.firstElementChild;
  while (listElem.children.length < dataSource.length + 1) {
    listElem.append(template.content.children[0].cloneNode(true));
  }
  while (listElem.children.length > dataSource.length + 1) {
    listElem.removeChild(listElem.lastElementChild);
  }
  for (let i = 0; i < dataSource.length; i++) {
    updateElem(listElem.children[i + 1], dataSource[i]);
  }
}

// See https://en.wikipedia.org/wiki/Video_file_format
const videoExtsLowerCase = new Set(['mkv', 'flv', 'vob', 'ogv', 'ogg', 'avi', 'mts', 'm2ts', 'ts', 'mov', 'qt', 'wmv', 'yuv', 'asf', 'amv', 'mp4', 'm4p', 'm4v', 'mpg', 'mp2', 'mpeg', 'mpe', 'mpv', 'm2v', 'svi', '3gp', '3g2', 'mxf', 'roq', 'nsv', 'webm']);

function fileExtOf(path) {
  const fileName = path.substring(path.lastIndexOf('/'));
  const index = fileName.lastIndexOf('.');
  if (index >= 0) {
    return fileName.substring(index + 1);
  } else {
    return '';
  }
}

function pathIsVideo(path) {
  return videoExtsLowerCase.has(fileExtOf(path).toLowerCase())
}
