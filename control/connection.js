const pairingCodeLength = 4;
const stableCodeLength = 8;
let messageQueue = [];
let cachedDisconnectReason = null;

let socket = null;

// Used for instramentation
let showLatency = false; // Can be set manually from console
let lastMessageType = null;
let lastMessageTime = 0;

addEventListener('beforeunload', () => {
  resetState();
});

function sendMessage(message) {
  if (socket && socket.readyState == WebSocket.OPEN) {
    if (showLatency) {
      lastMessageType = message.type;
      lastMessageTime = performance.now();
    }
    socket.send(JSON.stringify(message));
  } else {
    messageQueue.push(message);
  }
}

function clearToken() {
  localStorage.removeItem('token');
}

function hasToken() {
  return !!localStorage.getItem('token');
}

function disconnectExplicitly() {
  if (socket && (
    socket.readyState == WebSocket.OPEN ||
    socket.readyState == WebSocket.CONNECTING)
  ) {
    socket.close();
  }
  cachedDisconnectReason = '';
}

function onClose(reason) {
  // cachedDisconnectReason is only null if either no previous socket has been closed, or the most recent socket to
  // close got at least one message. If the most recent socket got zero messages it probably means it was created
  // by the autoconnect we are about to call and failed for the same reason the last one did. In this case we should
  // not try to auto-connect again.
  if (cachedDisconnectReason === null && reason !== 'connected from another tab') {
    cachedDisconnectReason = reason || '';
    tryAutoconnect();
  } else {
    resetState();
    const effectiveReason = cachedDisconnectReason || reason;
    if (effectiveReason) {
      setStatus('Disconnected, ' + effectiveReason);
    }
  }
}

function onMessage(message) {
  try {
    // Set cachedDisconnectReason to null here, to indicate if this socket closes we should try to reconnect
    cachedDisconnectReason = null;
    const parsed = JSON.parse(message);
    if (showLatency && lastMessageType !== null) {
      console.log(
        'Got ' + parsed.type + ' ' +
        (performance.now() - lastMessageTime) + 'ms after ' +
        lastMessageType
      );
      lastMessageType = null;
    }
    handleMessage(parsed);
    if (socket && socket.readyState == WebSocket.OPEN) {
      for (const queued of messageQueue) {
        sendMessage(queued);
      }
      messageQueue.length = 0;
    }
  } catch (err) {
    showError('Handling ' + message + ': ' + err);
  }
}

function tryAutoconnect() {
  if (socket && (
    socket.readyState == WebSocket.OPEN ||
    socket.readyState == WebSocket.CONNECTING)
  ) {
    return;
  }
  const token = localStorage.getItem('token');
  if (!token) {
    return;
  }
  setStatus('Connecting...');
  const prefix = (window.location.protocol === 'https:') ? 'wss://' : 'ws://';
  const url = prefix + window.location.host + '/websocket/' + token;
  socket = new WebSocket(url);
  socket.onclose = e => {
    socket = null;
    onClose(e.reason);
  };
  socket.onmessage = message => {
    onMessage(message.data);
  };
}

function tryPair(pairingCode) {
  if (!pairingCode.length) {
    return
  } else if (pairingCode.length != pairingCodeLength && pairingCode.length != stableCodeLength) {
    showError('Pairing code wrong length');
    return;
  }
  setStatus('Pairing...');
  fetch(new Request(
    window.location.origin + '/API/new_control_token/' + pairingCode,
    {method: 'POST'},
  )).then(response => {
    if (response.ok) {
      response.text().then(text => {
        if (text.startsWith('CTRL-')) {
          localStorage.setItem('token', text);
          tryAutoconnect();
        } else {
          showError('Got invalid token: ' + text);
        }
      })
    } else {
      response.text().then(text => {
        console.error('pairing error: ' + text);
        resetState();
        showError(text);
      });
    }
  }).catch(err => {
    setStatus('Error');
    console.error('Generating token', err);
  });
}

document.addEventListener("visibilitychange", () => {
  if (!document.hidden) {
    tryAutoconnect();
  }
});
