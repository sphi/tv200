let trackSelector = null;
const trackTypesMap = new Map();

function updateSelectTrackPopupElem() {
  if (!trackSelector) return;

  updateListContents(trackSelector.root, Array.from(trackTypesMap), (typeElem, [trackType, trackList]) => {
    typeElem.querySelector('#track-type').textContent = trackType + ':';
    updateListContents(typeElem.querySelector('#track-list'), trackList, (trackElem, track) => {
      const trackName =
        (track.selected ? '● ' : '○ ') +
        (typeof track.name === 'string' ? track.name : (track.name ? 'On' : 'Off'));
      trackElem.querySelector('#track-name').textContent = trackName;
      trackElem.onclick = () => {
        sendMessage({
          type: 'action',
          action: 'select-track',
          'track-type': track.type,
          'track-name': track.name,
        });
      };
    });
  });
}

function onTracksClicked() {
  onGrabsDismissed();
  trackSelector = {
    root: instantiateTemplate('track-selector'),
  }
  updateSelectTrackPopupElem();
  showPopup(trackSelector.root, 'Close');
}

function tracksUpdated() {
  trackTypesMap.clear();
  for (const track of mediaState.tracks ?? []) {
    const typeName = track.type.charAt(0).toUpperCase() + track.type.slice(1);
    if (!trackTypesMap.has(typeName)) {
      trackTypesMap.set(typeName, [])
    }
    trackTypesMap.get(typeName).push(track)
  }

  const tracksButton = document.getElementById('tracks-button');
  const firstTypeName = trackTypesMap.keys().next().value;
  tracksButton.style.display = firstTypeName ? 'block' : 'none';
  tracksButton.textContent = firstTypeName;
  updateSelectTrackPopupElem();
}
