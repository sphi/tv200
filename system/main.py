#!/usr/bin/env python3

from sanic import Sanic, Request, HTTPResponse, text, empty, json
import asyncio
from typing import Optional
import sys
import repo
import pathlib
from runner import Runner
import cec_manager
import vlc_manager
import local_files
import qbittorrent_manager
import user_config

app = Sanic('tv200-system')
shutting_down = False

def on_player_exit(code: int) -> None:
    global shutting_down
    if not shutting_down:
        app.stop()

@app.listener("before_server_start")
async def before_start(app: Sanic) -> None:
    await user_config.load()
    start_player_path = str(pathlib.Path(__file__).parent.parent / 'scripts/start-player.sh');
    Runner([start_player_path], on_exit=on_player_exit)
    asyncio.create_task(check_for_updates())

@app.listener("after_server_stop")
async def after_stop(app: Sanic) -> None:
    global shutting_down
    shutting_down = True
    await Runner.stop_all()

async def tv_power(state: bool) -> HTTPResponse:
    try:
        await user_config.set_tv_power(state);
        return empty()
    except Exception as e:
        return text(str(e), status=500)

async def tv_volume(steps: int) -> HTTPResponse:
    try:
        await user_config.set_tv_volume(steps)
        return empty()
    except Exception as e:
        return text(str(e), status=500)

async def spoof_input_event() -> HTTPResponse:
    await Runner(['xdotool', 'key', 'F10']).wait()
    return empty()

async def check_for_updates() -> str:
    if user_config.get_config_value('auto-updates'):
        if await repo.check_for_update():
            # Signal that the repo was updated, so the on-login script restarts us
            pathlib.Path('repo-updated').touch()
            app.stop()
            return 'Update installed'
        else:
            return 'No updates available'
    else:
        return 'Auto-updates disabled in config'

@app.route('/message', methods=["POST"])
async def message(request: Request):
    message: dict = request.json
    t: Optional[str] = message.get('type')
    if t == 'tv-power':
        return await tv_power(message.get('state') == True)
    elif t == 'tv-volume':
        steps = message.get('steps')
        if not isinstance(steps, int):
            return text('invalid volume steps: ' + repr(steps), status=400)
        return await tv_volume(steps)
    elif t == 'open-native':
        try:
            await vlc_manager.start(message['url'])
            return empty()
        except Exception as e:
            return text(str(e), status=500)
    elif t == 'close-native':
        await vlc_manager.stop()
        return empty()
    elif t == 'spoof-input-event':
        return await spoof_input_event()
    elif t == 'config-request':
        await user_config.load() # Alway reload, this request may be made because the user updated their config
        return json({
            'type': 'config-response',
            'config': user_config.loaded_config,
            'warnings': user_config.warning_messages,
        })
    elif t == 'local-files-request':
        response = local_files.get_listing()
        response['type'] = 'local-files-response'
        return json(response)
    elif t == 'set-torrent-running':
        if message.get('running'):
            await qbittorrent_manager.start()
        else:
            await qbittorrent_manager.stop()
        return empty()
    elif t == 'check-for-update':
        return text(await check_for_updates())
    else:
        return text('invalid message state ' + repr(t), status=400)

if __name__ == '__main__':
    # Support both to match the warning sanic gives automatically when run in production mode
    debug = '--debug' in sys.argv or '--dev' in sys.argv
    app.run(
        port=5056,
        single_process=True,
        access_log=debug,
        debug=debug
    )
