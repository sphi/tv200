import asyncio
import logging

async def change_vol(percent: int) -> None:
    sink = '@DEFAULT_SINK@'
    change = ('-' if percent < 0 else '+') + str(abs(percent)) + '%'
    process = await asyncio.create_subprocess_exec(
        'pactl', 'set-sink-volume', sink, change
    )
    await process.communicate()
    if process.returncode != 0:
        logging.error('pactl exited with code' + str(process.returncode))
