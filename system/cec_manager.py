from typing import Optional
from runner import Runner

cec_runner: Optional[Runner] = None

async def set_running(running: bool) -> None:
    global cec_runner
    if (cec_runner is not None) == running:
        return
    if cec_runner is None:
        # Will get stopped after server shutdown when all subprocesses are stopped
        # Times out and restarts once a week to mitigate memory leaks and such
        cec_runner = Runner(['cec-client', '-d', '1'], restart=True, timeout=7 * 24 * 60 * 60)
    else:
        await cec_runner.stop()
        cec_runner = None

async def send_cec_command(command: str) -> None:
    assert cec_runner is not None, 'CEC was not initialized'
    await cec_runner.send_input(command)

async def send_activate() -> None:
    # Turn on and set active source for address 0
    await send_cec_command('on 0\nas 0\n')

async def send_standby() -> None:
    # Turn off for address 0
    await send_cec_command('standby 0\n')

async def send_volup() -> None:
    await send_cec_command('volup\n')

async def send_voldown() -> None:
    await send_cec_command('voldown\n')
