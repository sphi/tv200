import pathlib

def get_project_root() -> str:
    return str(pathlib.Path(__file__).resolve().parent.parent)
