from typing import Optional
from runner import Runner
from user_config import get_config_value
import os

# For testing
# https://webtorrent.io/free-torrents

user_name = os.getlogin()
qbittorrent_runner: Optional[Runner] = None
max_run_seconds = 60 * 60 * 6 # 6 hours

async def start() -> None:
    global qbittorrent_runner
    if not get_config_value('torrenting'):
        raise RuntimeError('Torrenting is disabled in config')
    if qbittorrent_runner is not None:
        return
    command = ['qbittorrent-nox', '--webui-port=5057']
    netns_name = get_config_value('torrenting-netns')
    if netns_name is not None:
        command = [
            'sudo', 'ip', 'netns', 'exec', netns_name,
            'runuser', '-u', user_name, '--',
        ] + command
    qbittorrent_runner = Runner(command, timeout=max_run_seconds)

async def stop() -> None:
    global qbittorrent_runner
    if qbittorrent_runner is not None:
        await qbittorrent_runner.stop()
        qbittorrent_runner = None
