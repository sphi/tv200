from typing import Optional
import asyncio
import logging
import utils

async def get_latest_commit_internal() -> Optional[str]:
    process = await asyncio.create_subprocess_exec(
        'git', '-C', utils.get_project_root(), 'rev-parse', 'HEAD',
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
    )
    stdout, stderr = await process.communicate()
    if process.returncode != 0:
        logging.error(
            'getting latest commit hash exited with code '
            f'{process.returncode}: {stderr.decode().strip()}')
        return None
    return stdout.decode().strip()

async def get_latest_commit() -> Optional[str]:
    try:
        return await asyncio.wait_for(get_latest_commit_internal(), timeout=1)
    except asyncio.exceptions.TimeoutError:
        logging.error('getting latest commit hash timed out')
        return None

async def run_git_pull() -> None:
    process = await asyncio.create_subprocess_exec(
        'git', '-C', utils.get_project_root(), 'pull',
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
    )
    stdout, stderr = await process.communicate()
    if process.returncode != 0:
        logging.error(
            'git pull exited with code '
            f'{process.returncode}: {stderr.decode().strip()}')

async def check_for_update() -> bool:
    commit_before = await get_latest_commit()
    logging.info('pulling git')
    await run_git_pull()
    commit_after = await get_latest_commit()
    # Detect if there were updates
    return commit_after != commit_before
