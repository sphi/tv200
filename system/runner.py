from typing import Optional, Callable
import asyncio
import os
import signal
import sys
import shutil

class _Pipe:
    def __init__(self, on_data: Callable[[bytes], None]) -> None:
        self.on_data = on_data
        self.r_fd, self.w_fd = os.pipe()
        self.loop = asyncio.get_running_loop()
        assert self.loop, 'No running event loop'
        self.loop.add_reader(self.r_fd, self.data_ready)

    def close(self) -> None:
        self.loop.remove_reader(self.r_fd)
        os.close(self.w_fd)
        os.close(self.r_fd)
        try:
            self.on_data(bytes())
        except:
            pass

    def data_ready(self) -> None:
        data = os.read(self.r_fd, 1024) # Up to 1kb, function will be re-called if there's more data
        if len(data) > 0:
            try:
                self.on_data(data)
            except:
                pass

class StringCollector:
    def __init__(self, limit = 10000) -> None:
        self.text = ''
        self.limit = limit

    def __str__(self) -> str:
        return self.text

    def __call__(self, b: bytes) -> None:
        # I could do something very fancy to handle the case that a code point is split across multiple calls, but I
        # think that's rather unlikely so I'm not going to
        self.text += b.decode('utf-8', errors='ignore')
        if self.limit is not None and len(self.text) > self.limit * 10:
            self.text = self.text[-self.limit:]

def bytes_to_lines(cb: Callable[[str], None]) -> Callable[[bytes], None]:
    buf = bytes()
    def result(b: bytes) -> None:
        nonlocal buf
        lines = b.split(b'\n')
        lines[0] = buf + lines[0]
        if len(b) == 0:
            lines.append(bytes())
        for i in range(len(lines) - 1):
            try:
                cb(lines[i].decode('utf-8', errors='ignore'))
            except:
                pass
        buf = lines[-1]
    return result

class Runner:
    '''Runs and manages a subprocess'''
    restart_timeout = 0.5
    _active: list['Runner'] = []

    def __init__(self,
        args: list[str],
        restart: bool = False,
        timeout: Optional[float] = None,
        on_stdout: Optional[Callable[[bytes], None]] = None,
        on_stderr: Optional[Callable[[bytes], None]] = None,
        on_exit: Optional[Callable[[int], None]] = None
    ) -> None:
        '''
        args: command line arguments
        restart: if to automatically restart this subprocess if it exits without being explicitly stopped
        timeout: how long to let it run for, if both restart and timeout are set it automatically be stopped and started
            in a loop
        on_stdout: callback to be called when bytes of stdout are available. called with empty array once when no more
            data will be sent. Use an instance of StringCollector if you want the full output as a string, and wrap your
            callback in bytes_to_lines if you want to process output line-by-line as a string
        on_stderr: callback to be called when bytes of stdout are available
        on_exit: called each time the subprocess exits with the it's return code
        '''
        assert len(args)
        self.name = args[0]
        self.args = args
        self.restart = restart
        self.timeout = timeout
        self.stdout_pipe = _Pipe(on_stdout) if on_stdout is not None else None
        self.stderr_pipe = _Pipe(on_stderr) if on_stderr is not None else None
        self.on_exit = on_exit
        self.returncode: Optional[int] = None
        self.sigint_sent = False
        self._is_running = True
        self.process: Optional[asyncio.subprocess.Process] = None
        self.runner: Optional[asyncio.Task] = asyncio.create_task(self._run())
        Runner._active.append(self)

    async def _run(self) -> None:
        while not self.sigint_sent:
            if shutil.which(self.args[0]) is None:
                print(self.name + ' not available', file=sys.stderr)
                break
            if self.returncode is not None:
                print('restarting ' + self.name, file=sys.stderr)
            else:
                print('starting ' + self.name, file=sys.stderr)
            try:
                self.sigint_sent = False
                self.returncode = None
                # awaits process creation, not completion
                process = await asyncio.create_subprocess_exec(
                    *self.args,
                    stdin=asyncio.subprocess.PIPE,
                    stdout=self.stdout_pipe.w_fd if self.stdout_pipe is not None else None,
                    stderr=self.stderr_pipe.w_fd if self.stderr_pipe is not None else None,
                    preexec_fn=os.setpgrp)
                self.process = process
                task = asyncio.create_task(self.process.communicate())
                done, pending = await asyncio.wait({task}, timeout=self.timeout)
                if task in pending:
                    if self.restart:
                        print('restarting ' + self.name + ' due to timeout', file=sys.stderr)
                    else:
                        print('stopping ' + self.name + ' due to timeout', file=sys.stderr)
                    os.killpg(process.pid, signal.SIGINT)
                    done, pending = await asyncio.wait({task}, timeout=3)
                    if task in pending:
                        print(self.name + ' didn\'t exit, killing', file=sys.stderr)
                        os.killpg(process.pid, signal.SIGKILL)
                stdout, stderr = await task
                self.returncode = process.returncode
                print(self.name + ' exited with code ' + str(self.returncode), file=sys.stderr)
                if self.on_exit is not None:
                    self.on_exit(0 if self.returncode is None else self.returncode)
            except Exception as e:
                print('error running ' + self.name + ': ' + str(e), file=sys.stderr)
            self.process = None
            if self.sigint_sent:
                break
            if self.restart:
                print('restarting ' + self.name + ' in ' + str(Runner.restart_timeout) + ' seconds', file=sys.stderr)
                await asyncio.sleep(Runner.restart_timeout)
            else:
                break
        self._is_running = False
        if self.stdout_pipe is not None:
            self.stdout_pipe.close()
        if self.stderr_pipe is not None:
            self.stderr_pipe.close()

    async def send_input(self, text: str) -> None:
        assert self.process is not None, str(self.name) + ' is not running'
        assert self.process.stdin is not None, str(self.name) + ' stdin is not available'
        self.process.stdin.write(text.encode('utf-8'))
        await self.process.stdin.drain()

    def send_sigint(self) -> None:
        if self.process is not None:
            os.killpg(self.process.pid, signal.SIGINT)
        self.sigint_sent = True

    async def wait(self) -> None:
        if self.runner is None:
            return
        await self.runner
        self.runner = None
        try:
            Runner._active.remove(self)
        except ValueError:
            pass

    async def stop(self) -> None:
        if self.runner is None:
            return
        if self._is_running and not self.sigint_sent:
            self.send_sigint()
        done, pending = await asyncio.wait({self.runner}, timeout=3)
        if self.runner in pending and self.process is not None:
            print(self.name + ' didn\'t exit, killing', file=sys.stderr)
            os.killpg(self.process.pid, signal.SIGKILL)
        await self.wait()

    def __del__(self):
        if self.process is not None:
            print(self.name + ' was not stopped correctly', file=sys.stderr)
            os.killpg(self.process.pid, signal.SIGKILL)
            self.sigint_sent = True

    @staticmethod
    async def stop_all() -> None:
        subprocesses = list(reversed(Runner._active))
        for subprocess in subprocesses:
            try:
                subprocess.send_sigint()
            except Exception as e:
                print('error sending ' + subprocess.name + ' SIGINT: ' + str(e), file=sys.stderr)
        for subprocess in subprocesses:
            try:
                await subprocess.stop()
            except Exception as e:
                print('error stopping ' + subprocess.name + ': ' + str(e), file=sys.stderr)
