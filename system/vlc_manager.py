from typing import Optional
from runner import Runner

vlc_runner: Optional[Runner] = None

async def start(path: str) -> None:
    global vlc_runner
    if vlc_runner is not None:
        await vlc_runner.stop()
    vlc_runner = Runner([
        'vlc',
        '--fullscreen',
        '--intf', 'qt',
        '--extraintf', 'http',
        '--http-host', 'localhost',
        '--http-port', '9090',
        '--http-password', 'tv200',
        '--sub-track', '1000', # --sub-track -1 doesn't work but this does lol
        path
    ])

async def stop() -> None:
    global vlc_runner
    if vlc_runner is not None:
        await vlc_runner.stop()
        vlc_runner = None
