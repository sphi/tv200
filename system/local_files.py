import os
from typing import Optional
from user_config import get_config_value

def get_listing_for(path: str) -> Optional[dict]:
    if os.path.islink(path):
        return None
    elif os.path.isdir(path):
        return {
            'type': 'dir',
            'name': os.path.basename(path),
            'contents': get_contents_of_dir(path),
        }
    elif os.path.isfile(path):
        return {
            'type': 'file',
            'name': os.path.basename(path)
        }
    else:
        return None

def item_to_key(item):
    name = item['name'].lower() + '_'
    result = '1' if item['type'] == 'dir' else '2'
    number_accum = ''
    for c in name:
        if c.isnumeric():
            number_accum += c
        else:
            if number_accum:
                result += f'{int(number_accum):08d}'
            result += c
    return result

def get_contents_of_dir(path: str) -> list[dict]:
    ret = []
    for item in os.listdir(path):
        loaded = get_listing_for(f'{path}/{item}')
        if loaded is not None:
            ret.append(loaded)
        if len(ret) > 200:
            ret.append({
                'type': 'file',
                'name': '(additional files not shown)'
            })
            break
    ret = sorted(ret, key=item_to_key)
    return ret

def get_listing() -> dict:
    file_browsing_root = get_config_value('file-browsing-root')
    if file_browsing_root is None:
        return {
            'error': 'File browsing not configured',
            'root': None,
            'contents': [],
        }
    else:
        try:
            file_browsing_root = os.path.expanduser(file_browsing_root)
            return {
                'error': None,
                'root': file_browsing_root,
                'contents': get_contents_of_dir(file_browsing_root),
            }
        except Exception as e:
            return {
                'error': str(e),
                'root': file_browsing_root,
                'contents': [],
            }
