import utils
import os
import json
from typing import Any, Optional
import cec_manager
import pulse_integration

loaded_config: Optional[dict[str, Any]] = None
warning_messages: list[str] = ['Config not loaded']

async def load() -> None:
    global loaded_config
    global warning_messages
    warning_messages = []
    config = {}
    default_path = os.path.join(utils.get_project_root(), 'player', 'default-config.json')
    with open(default_path) as f:
        default = json.load(f)
        for key, value in default.items():
            if not key.startswith('//'):
                config[key] = value
    override_path = os.path.join(utils.get_project_root(), 'config.json')
    try:
        with open(override_path) as f:
            override = json.load(f)
            for key, value in override.items():
                if not key.startswith('//'):
                    if key in config:
                        config[key] = value
                    else:
                        warning_messages.append('Key ' + json.dumps(key) + ' exists in config but not default config')
    except FileNotFoundError:
        warning_messages.append('config.json does not exist (falling back to defaults)')
    except json.decoder.JSONDecodeError as e:
        warning_messages.append('Failed to load config.json: ' + str(e))
    loaded_config = config
    await cec_manager.set_running(get_needs_cec())

def get_needs_cec() -> bool:
    return get_config_value('power-mode') == 'cec' or get_config_value('volume-mode') == 'cec'

def get_config_value(key: str) -> Any:
    if loaded_config is None:
        raise RuntimeError('Config not loaded')
    if key not in loaded_config:
        raise RuntimeError('Invalid config key ' + json.dumps(key))
    return loaded_config[key]

async def set_tv_power(state: bool) -> None:
    mode = get_config_value('power-mode')
    if mode == 'cec':
        if state:
            await cec_manager.send_activate()
        else:
            await cec_manager.send_standby()
    elif mode == None:
        print('Power control not configured')
    else:
        raise RuntimeError('Invalid power-mode configured: ' + str(mode))

async def set_tv_volume(steps: int) -> None:
    mode = get_config_value('volume-mode')
    if mode == 'cec':
        iters = abs(steps)
        for i in range(iters):
            if steps > 0:
                await cec_manager.send_volup()
            else:
                await cec_manager.send_voldown()
    elif mode == 'pulse':
        await pulse_integration.change_vol(steps * 5)
    elif mode == None:
        print('Volume control not configured')
    else:
        raise RuntimeError('Invalid volume-mode configured: ' + str(mode))
