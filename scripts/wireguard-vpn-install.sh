#!/usr/bin/env bash
set -euo pipefail

if test $(id -u) != 0; then
  echo "Script must be run as root"
  exit 1
fi

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

CONF_PATH="/etc/wireguard/vpn-wg.conf"
if test ! -f $CONF_PATH; then
  TMP_DIR=/tmp/vpn
  rm -rf $TMP_DIR
  mkdir -p $TMP_DIR

  echo "Downloading mullvad files..."
  curl -o "$TMP_DIR/mullvad-wg.sh" "https://raw.githubusercontent.com/mullvad/mullvad-wg.sh/main/mullvad-wg.sh"
  curl -o "$TMP_DIR/mullvad-wg.sh.asc" "https://raw.githubusercontent.com/mullvad/mullvad-wg.sh/main/mullvad-wg.sh.asc"
  curl -o "$TMP_DIR/mullvad-code-signing.asc" "https://mullvad.net/media/mullvad-code-signing.asc"

  echo "Verifying mullvad files..."
  gpg --import "$TMP_DIR/mullvad-code-signing.asc"
  gpg --verify "$TMP_DIR/mullvad-wg.sh.asc"

  echo "Running mullvad config script..."
  chmod +x "$TMP_DIR/mullvad-wg.sh"
  "$TMP_DIR/mullvad-wg.sh"

  while test ! -f $CONF_PATH; do
    read -p 'Enter default: ' VPN_NAME
    if test -f "/etc/wireguard/${VPN_NAME}.conf"; then
      cp "/etc/wireguard/${VPN_NAME}.conf" $CONF_PATH
    else
      echo "Invalid"
    fi
  done

  # From https://mullvad.net/en/help/wireguard-and-mullvad-vpn
  KILL_SWITCH_LINES='PostUp  =  iptables -I OUTPUT ! -o %i -m mark ! --mark $(wg show %i fwmark) -m addrtype ! --dst-type LOCAL -j REJECT && ip6tables -I OUTPUT ! -o %i -m mark ! --mark $(wg show %i fwmark) -m addrtype ! --dst-type LOCAL -j REJECT
PreDown = iptables -D OUTPUT ! -o %i -m mark ! --mark $(wg show  %i fwmark) -m addrtype ! --dst-type LOCAL -j REJECT && ip6tables -D OUTPUT ! -o %i -m mark ! --mark $(wg show  %i fwmark) -m addrtype ! --dst-type LOCAL -j REJECT'
  KILL_SWITCH_LINES="${KILL_SWITCH_LINES//$'\n'/\\n}" # Replace newlines with \n
  awk "NR==2{print "'"'"$KILL_SWITCH_LINES"'"'"}1" $CONF_PATH >"$TMP_DIR/conf"
  mv "$TMP_DIR/conf" $CONF_PATH

  echo "Created $CONF_PATH"
  rm -rf "$TMP_DIR"
fi

# Set up DNS
echo "Setting up DNS for the network namespace..."
mkdir -p /etc/netns/vpn
echo "nameserver 1.1.1.1" > /etc/netns/vpn/resolv.conf
# Unclear if that did anything, or is correct. Mullvad seems to configure wireguard to set up it's own DNS (10.64.0.1).
# This is only accessable when connected to mullvad. Leaving this for now in case creating the file is somehow important.

echo "Installing services..."
systemctl is-active --quiet wireguard-vpn && systemctl stop wireguard-vpn || true
systemctl stop 'wireguard-vpn-port-forward@*' || true
mkdir -p /usr/local/lib/systemd/system
echo "[Install]
WantedBy=multi-user.target

[Unit]
Wants=network-online.target
After=network-online.target
Description=VPN WireGuard Tunnel

[Service]
ExecStart=$SCRIPT_DIR/wireguard-vpn-start.sh
ExecStopPost=$SCRIPT_DIR/wireguard-vpn-stop.sh
RemainAfterExit=true
Type=oneshot
" >/usr/local/lib/systemd/system/wireguard-vpn.service

echo "[Install]
WantedBy=multi-user.target

[Unit]
Wants=network-online.target
After=network-online.target
Description=Connects a given localhost port to the inside of the VPN netns

[Service]
ExecStart=socat 'TCP-LISTEN:%i,reuseaddr,fork' 'EXEC:ip netns exec vpn socat STDIO TCP-CONNECT\:127.0.0.1\:%i'
Type=simple
Restart=always
" >/usr/local/lib/systemd/system/wireguard-vpn-port-forward@.service

systemctl daemon-reload
systemctl enable --now wireguard-vpn
systemctl enable --now wireguard-vpn-port-forward@5057
echo "wireguard-vpn services installed and started"
