#!/usr/bin/env bash
set -euo pipefail

sudo ip netns exec vpn runuser -u "$USER" -- env XDG_RUNTIME_DIR="$XDG_RUNTIME_DIR" "$@"
