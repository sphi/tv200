#!/usr/bin/env bash
set -euo pipefail

# Some useful info on non-interactive pi configuration: https://raspberrypi.stackexchange.com/a/66939
# Probably best to consult the source: https://github.com/RPi-Distro/raspi-config/blob/master/raspi-config

# Prevent apt from prompting us
export DEBIAN_FRONTEND=noninteractive

# Directory this script is in
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
PROJECT_DIR="$( dirname "$SCRIPT_DIR" )"

echo "Updating system..."
sudo apt-get update
sudo apt dist-upgrade

echo "Enabling auto-updates..."
sudo apt-get install unattended-upgrades
sudo dpkg-reconfigure --priority=medium unattended-upgrades

echo "Installing desktop environment..."
sudo apt install xserver-xorg lightdm raspberrypi-ui-mods libgles2-mesa libgles2-mesa-dev xorg-dev xdotool
sudo systemctl enable lightdm

echo "Removing xscreensaver..."
sudo apt remove xscreensaver

echo "Setting auto-login to desktop"
# See https://github.com/RPi-Distro/raspi-config/blob/10431dc73d05031788a2f6a8981cf8c4ef9d0f33/raspi-config#L1398
# If this stops working, check if raspi-config has changed
AUTO_LOGIN_CODE=$(grep -Po 'B\d(?= Desktop Autologin")' "$(which raspi-config)")
sudo raspi-config nonint do_boot_behaviour "$AUTO_LOGIN_CODE"

MEM_SPLIT=256
echo "Setting GPU memory split to $MEM_SPLIT"
sudo raspi-config nonint do_memory_split "$MEM_SPLIT"

echo "Installing various packages..."
sudo apt install vim vlc arandr chromium-browser cec-utils python3-pip git jq wireguard qbittorrent-nox
sudo pip install sanic

echo "Setting git pull strategy..."
# This needs to be set so git pull works in the auto-update flow
git config --global pull.rebase true

echo "Looking for HDMI audio sink, if this errors you may need to reboot"
HDMI_SINK="$(pacmd list-sinks | grep 'name:' | grep -Po '(?<=<).*hdmi.*(?=>)')"
echo "Setting audio out to $HDMI_SINK"
pacmd set-default-sink "$HDMI_SINK"

if test -e "$PROJECT_DIR/config.json"; then
  echo "config.json already exists, leaving it alone"
else
  echo "Symlinking winter-config.json to config.json"
  ln -s "$PROJECT_DIR/winter-config.json" "$PROJECT_DIR/config.json"
fi

AUTOSTART_SCRIPT_PATH="/etc/profile.d/tv200.sh"
echo "Creating autostart script at $AUTOSTART_SCRIPT_PATH"
echo "$SCRIPT_DIR/on-login.sh &" | sudo tee "$AUTOSTART_SCRIPT_PATH"

echo "Setting timezone based on IP address"
sudo timedatectl set-timezone "$(curl http://ip-api.com/json/ | grep -Po '(?<="timezone":")[^"]*')"

echo "Installing VPN service..."
sudo "$SCRIPT_DIR/wireguard-vpn-install.sh"

echo "Configuring qBittorrent..."
QBT_CONF_PATH="$HOME/.config/qBittorrent/qBittorrent.conf"
if test ! -e "$QBT_CONF_PATH"; then
  touch "$QBT_CONF_PATH";
fi
# This should merge with any existing settings without issue next time qBt is run
echo "

[LegalNotice]
Accepted=true

[Preferences]
WebUI\Port=5057
WebUI\Address=127.0.0.1
WebUI\CSRFProtection=false
WebUI\LocalHostAuth=false
" >>"$QBT_CONF_PATH"

echo "
Next you might want to set the display resolution to 1080p.

Remember to add extensions to chromium:
  - ublock:  https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm
  - h264ify: https://chrome.google.com/webstore/detail/h264ify/aleakchihdccplidncghkekgioiakgal

And enable these flags:
  - chrome://flags/#ignore-gpu-blocklist
  - chrome://flags/#enable-gpu-rasterization

And finally, always launch chromium with:
  - --enable-features=VaapiVideoDecoder

You can test if hardware accel is working at chrome://gpu.
Make sure everything is green except vulkan.

You may want to reboot"
