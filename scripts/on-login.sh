#!/usr/bin/env bash
set -euo pipefail

# For some reason LXTerminal when run with a command counts as a login and runs this script again,
# so to prevent infinite terminals we check if a terminal is already running.
if pidof lxterminal >/dev/null; then
  # Already running
  exit
fi

# Move into project directory
cd "$( dirname "${BASH_SOURCE[0]}" )/.."

# Wait to give the X server time to start up
sleep 2

# Prevent screen locking
# (from https://forums.raspberrypi.com/viewtopic.php?t=147311)
xset s 0 0
xset s noblank
xset s noexpose
xset dpms 0 0 0

while true; do
  # --no-remote forces a new instance of LXTerminal to open. When it opens a new window on an existing
  # instance and the exisitng instance was launched from a symlink (eg /usr/bin/x-terminal-emulator)
  # then the pidof check for lxterminal at the top will fail.
  lxterminal --no-remote -e "./system/main.py"
  # When the repo-updated exists, it's a flag to indicate the program shut down because it completed an auto-update,
  # and it wants to be restarted. Otherwise, it actually wants to quit.
  if test ! -f repo-updated; then
    break
  fi
  rm repo-updated
done
