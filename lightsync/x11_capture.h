#pragma once

#include <X11/Xlib.h>
#include <stdbool.h>

#define my me->

typedef struct X11Capture {
    bool is_valid;
    Display* display;
    Window root;
    XImage* image;
} X11Capture;

X11Capture x11_capture_make();
void x11_capture_grab(X11Capture* me);
void x11_capture_sample(X11Capture* me, float x, float y, unsigned char* r, unsigned char* g, unsigned char* b);
void x11_capture_drop(X11Capture* me);
