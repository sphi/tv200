#define _DEFAULT_SOURCE

#include "wled_scanner.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#define MDNS_PORT 5353

// A lot of this is Claude's work fyi
// mDNS query packet for _wled._tcp.local
static unsigned char const query_packet[] = {
    0x00, 0x00, // Transaction ID (0 for mDNS)
    0x00, 0x00, // Flags (standard query)
    0x00, 0x01, // Questions count
    0x00, 0x00, // Answer count
    0x00, 0x00, // Authority count
    0x00, 0x00, // Additional count

    // Query for _wled._tcp.local
    0x05, '_', 'w', 'l', 'e', 'd',
    0x04, '_', 't', 'c', 'p',
    0x05, 'l', 'o', 'c', 'a', 'l',
    0x00,       // Null terminator

    0x00, 0x0c, // Type (PTR)
    0x00, 0x01  // Class (IN)
};

static int open_mdns_socket() {
    int mdns_socket;
    // Create UDP socket
    if ((mdns_socket = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        perror("socket");
        return -1;
    }

    // Allow multiple sockets to use the same port
    int yes = 1;
    if (setsockopt(mdns_socket, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) < 0) {
        perror("setsockopt (SO_REUSEADDR)");
        close(mdns_socket);
        return -1;
    }

    // Bind to mDNS port
    struct sockaddr_in addr = {
        .sin_family = AF_INET,
        .sin_port = htons(MDNS_PORT),
        .sin_addr.s_addr = htonl(INADDR_ANY),
    };

    if (bind(mdns_socket, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
        perror("bind");
        close(mdns_socket);
        return -1;
    }

    // Join multicast group
    struct ip_mreq mreq = {
        .imr_multiaddr.s_addr = inet_addr("224.0.0.251"),
        .imr_interface.s_addr = htonl(INADDR_ANY),
    };
    if (setsockopt(mdns_socket, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq)) < 0) {
        perror("setsockopt (IP_ADD_MEMBERSHIP)");
        close(mdns_socket);
        return -1;
    }

    // Set up multicast address for sending
    struct sockaddr_in multicast_addr = {
        .sin_family = AF_INET,
        .sin_port = htons(MDNS_PORT),
        .sin_addr.s_addr = inet_addr("224.0.0.251"),
    };

    // Send query
    if (sendto(mdns_socket, query_packet, sizeof(query_packet), 0,
            (struct sockaddr*)&multicast_addr, sizeof(multicast_addr)) < 0
    ) {
        perror("sendto");
        close(mdns_socket);
        return -1;
    }

    // Receive and process responses
    struct timeval tv = {
        .tv_sec = 1, // Timeout
    };
    setsockopt(mdns_socket, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));

    return mdns_socket;
}

static ssize_t http_get_request(const char* ip, const char* path, char* buffer, int buf_size) {
    int tcp_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (tcp_socket < 0) {
        perror("Socket creation failed");
        return -1;
    }

    // Set receive timeout
    struct timeval tv = {
        .tv_sec = 1,
    };
    if (setsockopt(tcp_socket, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0) {
        perror("Failed to set receive timeout");
        close(tcp_socket);
        return -1;
    }

    // Set send timeout
    if (setsockopt(tcp_socket, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(tv)) < 0) {
        perror("Failed to set send timeout");
        close(tcp_socket);
        return -1;
    }

    struct sockaddr_in server_addr;
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(80);

    // Convert IP address from string to binary form
    if (inet_pton(AF_INET, ip, &server_addr.sin_addr) <= 0) {
        perror("Invalid address");
        close(tcp_socket);
        return -1;
    }

    // Connect to the server
    if (connect(tcp_socket, (struct sockaddr*)&server_addr, sizeof(server_addr)) < 0) {
        perror("Connection failed (timeout after 5 seconds)");
        close(tcp_socket);
        return -1;
    }

    // Prepare HTTP GET request
    char request[4096];
    snprintf(request, sizeof(request),
        "GET %s HTTP/1.1\r\n"
        "Host: %s\r\n"
        "Connection: close\r\n"
        "\r\n",
        path, ip
    );

    // Send the request
    if (send(tcp_socket, request, strlen(request), 0) < 0) {
        perror("Send failed (timeout after 5 seconds)");
        close(tcp_socket);
        return -1;
    }

    // Receive response
    ssize_t bytes_received = recv(tcp_socket, buffer, buf_size - 1, 0);
    if (bytes_received < 0) {
        perror("Receive failed (timeout after 5 seconds)");
        close(tcp_socket);
        return -1;
    }

    buffer[bytes_received] = '\0';
    close(tcp_socket);
    return bytes_received;
}

static char* extract_str(const char* buffer, const char* start, const char* end) {
    const char* start_pos = strstr(buffer, start);
    if (start_pos) {
        start_pos += strlen(start);
        const char* end_pos = strstr(start_pos, end);
        if (end_pos) {
            ssize_t len = end_pos - start_pos;
            char* ret = malloc(len + 1);
            memcpy(ret, start_pos, len);
            ret[len] = 0;
            return ret;
        }
    }
    return NULL;
}

static WledInfo* append_info(WledInfo* head, const char* ip_addr, const char* buffer) {
    char* name = extract_str(buffer, "\"name\":\"", "\"");
    if (name) {
        char* udp_port_str = extract_str(buffer, "\"udpport\":\"", ",");
        int udp_port = udp_port_str ? atoi(udp_port_str) : 21324;
        free(udp_port_str);

        WledInfo* info = malloc(sizeof(WledInfo));
        fprintf(stderr, "Found WLED device '%s'\n", name);
        *info = (WledInfo) {
            .name = name,
            .udp_port = udp_port,
            .next = head
        };
        strcpy(info->ip_addr, ip_addr);
        return info;
    }
    fprintf(stderr, "Not a WLED device\n");
    return head;
}

WledInfo* wled_scanner_scan() {
    fprintf(stderr, "Scanning for WLED devices\n");

    int mdns_socket = open_mdns_socket();
    if (mdns_socket < 0) return NULL;

    WledInfo* head = NULL;

    while (1) {
        char buffer[8192];
        struct sockaddr_in sender_addr;
        socklen_t sender_addr_len = sizeof(sender_addr);

        ssize_t udp_received = recvfrom(mdns_socket, buffer, sizeof(buffer), 0, (struct sockaddr*)&sender_addr, &sender_addr_len);

        if (udp_received < 0) {
            break;  // Timeout or error
        }

        // Get the sender's IP address
        char sender_ip[INET_ADDRSTRLEN];
        inet_ntop(AF_INET, &(sender_addr.sin_addr), sender_ip, INET_ADDRSTRLEN);
        fprintf(stderr, "Got mDNS response from %s\n", sender_ip);

        ssize_t http_received = http_get_request(sender_ip, "/json/info", buffer, sizeof(buffer));
        if (http_received >= 0) {
            head = append_info(head, sender_ip, buffer);
        }
    }
    fprintf(stderr, "Scan finished\n");

    close(mdns_socket);

    return head;
}

static void info_drop(WledInfo* info) {
    free(info->name);
}

void wled_scanner_destroy_list(WledInfo* head) {
    while (head) {
        info_drop(head);
        WledInfo* prev = head;
        head = head->next;
        free(prev);
    }
}
