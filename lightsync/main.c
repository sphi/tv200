#define _POSIX_C_SOURCE 200809L
#include <time.h>
#include <stdio.h>
#include <string.h>

#include "x11_capture.h"
#include "wled_scanner.h"
#include "wled_transmitter.h"

static const char* device_name = "TV backlight";
static const int led_height = 52;
static const int led_width = 86;
static const float sample_inset = 0.02;

void sleep_ms(int ms) {
    int s = ms / 1000;
    struct timespec ts = {
        .tv_sec = s,
        .tv_nsec = (ms - (s * 1000)) * 1000000,
    };
    nanosleep(&ts, NULL);
}

int main() {
    X11Capture capture = x11_capture_make();
    if (!capture.is_valid) {
        return 1;
    }

    WledTransmitter wled = {0};
    WledInfo* info_head = wled_scanner_scan();
    WledInfo* info = info_head;
    while (info) {
        if (strcmp(info->name, device_name) == 0) {
            fprintf(stderr, "Selected to %s (%s)\n", info->name, info->ip_addr);
            wled = wled_transmitter_make(info->ip_addr, info->udp_port, led_height * 2 + led_width * 2);
            break;
        }
        info = info->next;
    }
    wled_scanner_destroy_list(info_head);

    while (capture.is_valid && wled.is_valid) {
        unsigned char r = 0, g = 0, b = 0;
        int index = 0;

        x11_capture_grab(&capture);

        for (int i = 0; i < led_height; i++) {
            float j = (float)i / (led_height - 1);
            x11_capture_sample(&capture, sample_inset, 1 - j, &r, &g, &b);
            wled_transmitter_set_colors(&wled, index, r, g, b);
            index++;
        }

        for (int i = 0; i < led_width; i++) {
            float j = (float)i / (led_width - 1);
            x11_capture_sample(&capture, j, sample_inset, &r, &g, &b);
            wled_transmitter_set_colors(&wled, index, r, g, b);
            index++;
        }

        for (int i = 0; i < led_height; i++) {
            float j = (float)i / (led_width - 1);
            x11_capture_sample(&capture, 1 - sample_inset, j, &r, &g, &b);
            wled_transmitter_set_colors(&wled, index, r, g, b);
            index++;
        }

        for (int i = 0; i < led_width; i++) {
            float j = (float)i / (led_width - 1);
            x11_capture_sample(&capture, 1 - j, 1 - sample_inset, &r, &g, &b);
            wled_transmitter_set_colors(&wled, index, r, g, b);
            index++;
        }

        wled_transmitter_send(&wled);
        sleep_ms(100);
    }
    wled_transmitter_drop(&wled);
    x11_capture_drop(&capture);
    return 0;
}
