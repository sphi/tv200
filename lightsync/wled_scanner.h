#pragma once

#include <netinet/in.h>

typedef struct WledInfo WledInfo;

typedef struct WledInfo {
    char* name;
    char ip_addr[INET_ADDRSTRLEN];
    int udp_port;
    WledInfo* next;
} WledInfo;

WledInfo* wled_scanner_scan();
void wled_scanner_destroy_list(WledInfo* head);
