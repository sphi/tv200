#include "x11_capture.h"

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <stdio.h>
#include <stdlib.h>

X11Capture x11_capture_make() {
    Display* display = XOpenDisplay(NULL);
    if (!display) {
        fprintf(stderr, "Cannot open X11 display\n");
        return (X11Capture){ .is_valid = false };
    }

    Window root = DefaultRootWindow(display);

    return (X11Capture) {
        .is_valid = true,
        .display = display,
        .root = root,
    };
}

void x11_capture_grab(X11Capture* me) {
    if (!my is_valid) return;

    XWindowAttributes attributes;
    XGetWindowAttributes(my display, my root, &attributes);

    if (my image) XDestroyImage(my image);

    my image = XGetImage(
        my display, my root,
        0, 0, attributes.width, attributes.height,
        AllPlanes, ZPixmap
    );

    if (!my image) {
        fprintf(stderr, "Failed to capture screenshot\n");
    }
}

void x11_capture_get_size(X11Capture* me, int* width, int* height) {
    if (!my is_valid || !my image) return;
    *width = my image->width;
    *height = my image->height;
}

void x11_capture_sample(X11Capture* me, float x, float y, unsigned char* r, unsigned char* g, unsigned char* b) {
    if (!my is_valid || !my image) return;

    int x_pix = x * (my image->width - 1);
    int y_pix = y * (my image->height - 1);

    if (x_pix < 0) x_pix = 0;
    if (x_pix >= my image->width) x_pix = my image->width;
    if (y_pix < 0) y_pix = 0;
    if (y_pix >= my image->height) y_pix = my image->height;

    unsigned long pixel = XGetPixel(my image, x_pix, y_pix);
    *r = (pixel & my image->red_mask) >> 16;
    *g = (pixel & my image->green_mask) >> 8;
    *b = (pixel & my image->blue_mask);
}

void x11_capture_drop(X11Capture* me) {
    if (my image) XDestroyImage(my image);
}
