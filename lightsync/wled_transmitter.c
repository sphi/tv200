#include "wled_transmitter.h"

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/socket.h>

static void wled_transmitter_open_socket(WledTransmitter* me) {
    if (!my is_valid) return;

    // Create UDP socket
    if (my socket_fd >= 0) {
        close(my socket_fd);
    }
    my socket_fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (my socket_fd < 0) {
        perror("Failed to create socket");
        my is_valid = false;
    }
}

WledTransmitter wled_transmitter_make(const char* ip_addr, int port, int led_count) {
    // Configure destination address
    struct sockaddr_in socket_address = {
        .sin_family = AF_INET,
        .sin_port = htons(port),
    };

    // Convert IP address from string to binary form
    if (inet_pton(AF_INET, ip_addr, &socket_address.sin_addr) <= 0) {
        perror("Invalid address");
        return (WledTransmitter){ .is_valid = false };
    }

    int message_length = 2 + led_count * 3;
    unsigned char* message_data = calloc(1, message_length);
    message_data[0] = 2; // DRGB protocol
    message_data[1] = 5; // max seconds to wait between packets
    if (!message_data) {
        fprintf(stderr, "Failed to allocate memory\n");
        return (WledTransmitter){ .is_valid = false };
    }

    return (WledTransmitter){
        .is_valid = true,
        .led_count = led_count,
        .message_length = message_length,
        .message_data = message_data,
        .socket_fd = -1,
        .socket_address = socket_address,
    };
}

void wled_transmitter_set_colors(WledTransmitter* me, int index, unsigned char r, unsigned char g, unsigned char b) {
    if (!my is_valid) return;
    if (index >= my led_count) return;
    my message_data[2 + index * 3 + 0] = r;
    my message_data[2 + index * 3 + 1] = g;
    my message_data[2 + index * 3 + 2] = b;
}

void wled_transmitter_send(WledTransmitter* me) {
    if (!my is_valid) return;
    // We really shouldn't have to do this every time, but otherwise it doesn't work
    wled_transmitter_open_socket(me);
    if (!my is_valid) return;
    ssize_t sent_bytes = sendto(
        my socket_fd,
        my message_data,
        my message_length,
        0,
        (struct sockaddr *)&my socket_address,
        sizeof(my socket_address)
    );

    if (sent_bytes < 0) {
        perror("Failed to send message");
        wled_transmitter_drop(me);
    }
}
void wled_transmitter_drop(WledTransmitter* me) {
    if (!my is_valid) return;
    free(my message_data);
    close(my socket_fd);
    my is_valid = false;
}
