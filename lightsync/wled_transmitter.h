#pragma once

#include <arpa/inet.h>
#include <stdbool.h>

#define my me->

typedef struct WledTransmitter {
    bool is_valid;
    int led_count;
    int message_length;
    unsigned char* message_data;
    int socket_fd;
    struct sockaddr_in socket_address;
} WledTransmitter;

WledTransmitter wled_transmitter_make(const char* ip_addr, int port, int led_count);
void wled_transmitter_set_colors(WledTransmitter* me, int index, unsigned char r, unsigned char g, unsigned char b);
void wled_transmitter_send(WledTransmitter* me);
void wled_transmitter_drop(WledTransmitter* me);
