# tv200 Documentation
This document defines the pieces that make up the tv200 smart TV stack, and the messages that let them talk to each other

## Overview
There is one Player device, which drives the TV. The `player` sub-project runs as a browser extension on Chrome/Chromium on the player device. It is split into three subdirectories: `home` (the home screen displayed when nothing is playing, and communication with the backend), `background` (the background service worker, not much logic here) and `content` (the scripts that run within the context of and interface with content websites like YouTube, Pandora, etc).

The `control` sub-project is a web app. Zero or more phones and computers running control connect to the player. They can give the player a URL to play media from, play/pause the current media, etc.

Android devices can install the `android` app to access the `control` web app instead of using a browser. This enables improved ergonomics, though most of the code and most of the functionality is the same (it's basically just a webview with features added where needed).

The `backend` sub-project runs on a web server accessible to all devices. Generally the one at `tv.phie.me` is used. It serves the `control` web app and handles websocket connections to both controls and players. It can handle multiple players at once, each with multiple controls.

The `system` sub-project is a server that optionally runs locally on the player device. It allows the player (which is sandboxed in the browser) to do things like issue HDMI CEC commands, run VLC, etc.

The `tv200-cli` script is a BASH script that allows you to cast and control media from the command line. It's promarily intended for scripting (for example, you could use it to make keyboard shortcuts on your computer change the TV volume). Run `./tv200-cli help` for full usage.

### Configuration
Users can set configuration options in a `config.json` file in the repository root of the player device. To use a custom config the `system` component must be working (if you *must* edit the config without `system` running you can edit the default config file). See the readme for documentation of specific config options.

### Stable pairing codes
The normal displayed pairing code changes when the browser restarts or loses connection to the server. The stable pairing code is longer, and doesn't. Either pairing code can also be used as the `code` a query param for a link to the control app, but the stable code is better for links that are harder to change (for example it could be used in a QR, I use it in an NFC tag). You can display your stable pairing by (on the player device) pressing `ESC` and then `S`.

### Browser compatibility
The player is a manifest v3 extension, so at the time of writing it's only supported by Chromium-based browsers. When Firefox supports manifest v3 it should work fine in it. The control app works on all modern browsers.

### Chrome store
There are no plans to upload the player extension to the Chrome store, and it's unclear if it would be accepted. Instead, use `system/main.py` to launch Chrome with the player loaded, or load the extension manually.

### Account sign-in
Signing into YouTube, Pandora, etc might be helpful or required to play media on those services. You can sign in to your account on the player device normally. There are no special features to facilitate this.

## Player
Player is a manifest v3 Chrome extension. It manages connecting to control apps, opening tabs with media, playing, pausing, etc. You can run Chrome with the extension inside it with `scripts/start-player.sh`. Alternatively, if this doesn't work or you're not on Linux:
- Go to `chrome://extensions/`
- Turn on `Developer mode`
- Click `Load unpacked` to load the extension from the `player` directory

### Home
The home tab is shown when the extension is initially loaded, and remains open as long as tv200 is active. It's script manages all long-lived state and objects including the websocket connection (doing this in the background script doesn't work well in manifest v3). The home script sends messages to the background script to open tabs, and to the current media tab directly to control playback.

### Background
Manifest v3 heavily nerfs the background script, so we use it for as little as possible. Mostly it handles messages from home relating to opening, closing and switching tabs.

### Content
This script runs in the context of the pages with media that we're playing (ex YouTube). It does things like removing non-video elements from the page, handling messages from home relating to playing and pausing, etc.

## Backend HTTP API
The HTTP API is used to set up a websocket connection.

### [GET] /
Static content for the control web app.

### [POST] /API/new_player_token
Returns a token that can be used by a player to connect.

### [POST] /API/new_control_token/<pairing_code>
Returns a token that can be used by a control to connect.

### [POST] /API/oneshot/<token>?timeout=<seconds>
This endpoint is likely to change in the future, if for example we switch to a long-polling system.

Opens a short-lived connection. The request body should contain an array of messages to send, and the response will be an array of messages it got back. Timeout is optional, and if not specified zero timeout is used (you still get the initial startup messages). Response is streamed as messages come in.

### [GET] /websocket/<token>
Opens a websocket connection for the given token (either control or player)

## Messages
Messages are sent between the various components. All messages are JSON objects, and have a `type` field with a string value that defines how the message should be interpreted.

### `authorized`
Indicates connecting was successful. May also be resent if the pairing code changes.

Sent between:
- Backend -> Home
- Backend -> Control

Properties:
- `code`: the current pairing code. Redundant if the control just paired, but useful if the control auto-connected. It is shown so that other devices may be paired using it.
- `stable`: an alternative pairing code that's longer and doesn't change when the player reconnects, only sent to home, not controls
- `commit`: the latest git commit hash of the backend

### `error`
Informs the device it made a non-fatal protocol error.

Sent between:
- Backend -> Home
- Backend -> Control

Properties:
- `message`: human readable message

### `control_count`
Gives the player the current number of connected controls so this can be displayed.

Sent between:
- Backend -> Home

Properties:
- `count`: number of controls

### `open-tab`
Open a new tab with a given URL.

NOTE: the background script responds with the tab ID, most messages to not have responses

Sent between:
- Home -> Background

Properties:
- `url`: the URL to open and look for media on

### `close-all`
Request that all media or unknown tabs are closed, returning to the home screen.

Sent between:
- Home -> Background

Properties: none

### `close-tab`
Request that a specific media tab be closed

Sent between:
- Home -> Background

Properties:
- `tabId`: the media tab to close

### `quit`
Quit the browser and return to desktop

Sent between:
- Home -> Background

Properties: none

### `ready`
Indicates that a tab has been loaded and should be switched to.

Sent between:
- Content -> (Background, Home)
- Background -> Home

Properties:
- `tabId`: only present when sent from Background to Home, implied by the sender tab ID when sent from Content to Background

### `prepare-close`
Sent to indicate the tab is about to be closed, and the tab should prepare for that if required.

Sent between:
- Home -> Content

Properties: none

### `state`
The current state of the player, all fields are optional, and should be considered unchanged if `undefined`. The complete state is sent when a player connects.

Sent between:
- Content -> (Background, Home)
- Home    -> Backend
- Backend -> Control

Properties:
- `queue`: array of media objects that are played in order. If queue is empty no media is playing. If queue is non-empty the 0th item is playing.
- `playing`: `true` if playing, false if paused, ended, or not applicable
- `length`: length of the media in seconds, or `null` if not applicable/unknown
- `progress`: current progress through the media in seconds, or `null` if not applicable/unknown
- `speed`: playback speed, or `null` if not applicable/unkown, 1.0 is normal
- `actions`: list of supported actions
- `tracks`: list of track objects, generally used for subtitles

Media object properties:
- `url`: URL of the media
- `name`: Name of the media, or `null` if unknown
- `id`: Arbitrary ID of the media. Note that if the same URL is queued multiple times each will have a different ID

Track object properties:
- `type`: Type of track, `audio`, `subtitles` or `lyrics`
- `name`: Name of track, or boolean for enabled/disabled (both sorts of names may appear for a single type)
- `selected`: Boolean, if this track is currently selected (exactly one of each available type should be selected)

### `enqueue`
Add media to the queue

Sent between:
- Control -> Backend
- Backend -> Home

Properties:
- `url`: URL of the media
- `id`: media ID, generated by the control. Control uses sufficiently long random string that collisions are unlikely. Collisions may cause strange behavior, but should not result in crashes or security issues.
- `after`: ID of the media to add this after, or `null` to override the current media/add to beginning of queue

### `move-media`
Set the position in the queue of a single piece of media

Sent between:
- Control -> Backend
- Backend -> Home

Properties:
- `id`: media ID of thing to move
- `after`: media to put this after, or `null` to override the current media/add to beginning of queue

### `drop-media`
Remove one or more medias from the queue

Sent between:
- Control -> Backend
- Backend -> Home

Properties:
- `ids`: array of media IDs to drop

### `action`
A media-control action that should eventually be passed to the current content script.

Sent between:
- Control -> Backend
- Backend -> Home
- Home    -> Content

Properties:
- `action`: determines the sub-action, see below

Actions:
- `toggle-play`: toggle the play/pause state of the media
- `seek`: seek to a particular point in the media
  - `time`: time (in seconds) to seek to
- `next`: next song or video
- `prev`: previous song or video
- `select-track`: select an audio track/set subtitles
  - `track-type`: type of track, from the track object
  - `track-name`: name of track, from the track object

### `open-native`
Play the given media using native commands.

Sent between:
- Home -> System

Properties:
- `url`: URL of the media to play

### `close-native`
Close any native media players/downloaders that are running.

Sent between:
- Home -> System

Properties: none

### `tv-volume`
Adjust the volume of the TV (not the OS or browser volume)

Sent between:
- Control -> Backend
- Backend -> Home
- Home    -> System

Properties:
- `steps`: can be negative, number of steps up or down to change it by

### `tv-power`
Turn the TV on or off (turning it on also swiches the current connection to be the active input)

Sent between:
- Control -> Backend
- Backend -> Home
- Home    -> System

Properties:
- `state`: `true` to turn the TV on or `false` to turn it off

### `spoof-input-event`
Get a mock input event (specifically the `F10` key), which is needed so the browser thinks it's been interacted with

Sent between:
- Content -> (Background, Home)
- Home -> System

Properties: none

### `local-files-request`
Request the directory structure of media files downloaded locally on player device.

Sent between:
- Control -> Backend
- Backend -> Home
- Home    -> System

Properties: none

### `local-files-response`
Response to a `local-files-request`. Note that only the control which makes the request gets the response (this is handled by the backend).

Sent between:
- System  -> Home (via response to `local-files-request`)
- Home    -> Backend
- Backend -> Control

Properties:
- `error`: either null, or a string error message
- `root`: file path of the root directory that media files are found in (such as /home/pi/Downloads), or null if there was a fatal error
- `contents`: array of files and directories

File properties:
- `type`: `'file'`
- `name`: file name

Directory properties:
- `type`: `'dir'`
- `name`: directory name
- `contents`: array of files and directories

### `set-torrent-running`
Starts or stops an instance of qbittorrent, if the requested state already matches the current state does nothing.

Sent between:
- Home -> System

Properties:
- `running`: boolean

### `config-request`
Request the user configuration, may signal that the user has requested the config to be reloaded.

Sent between:
- Home -> System

Properties: none

### `config-response`
User configuration.

Sent between:
- System -> Home

Properties:
- `config`: object containing the configuration
- `warnings`: a list of warning messages to display (`config` can be used even if not empty)

### `check-for-update`
Triggers the system to check for updates, it will restart the chrome instance if it finds anything

Sent between:
- Home -> System

Properties: none
